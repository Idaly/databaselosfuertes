<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
        	<h1 class="text-center login-title">Login to Your Account</h1><br>
            <div class="account-wall">
			
		
	
		<?php $current_url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		?>
		
		<form  class="form-signin" action="login.php" method="post">
			<div class="form-group">
			<table >
			<tr>
			<td id="username"  >Username:</td>
			<td id="username"><input  class="form-control" type="text" name ="username" value="" placeholder="Username" required autofocus></td>
			</tr>
					
			<tr>
			<td id="password" >Password:</td>
			<td id = "password"><input  class="form-control" type="password" name="password" value="" placeholder="Password" required></td>
			</tr>
			</table>
			</div>	
			<input type="hidden" name="return_url" value= <?php echo $current_url; ?>; />
			<input type="hidden" name="type" value="logIn" />
			<button   class="btn btn-lg btn-primary btn-block" type="submit" id ="logInSubBtn" name = "btnSubmit"  class="bttn-std"  >Log In</button></br></br>
			
			
			</form>
			<div class="login-help">
				<p>Forgotten your <a href ="recover.php?mode=username">Username </a>Or <a href ="recover.php?mode=password">Password</a>? </p>			
			 </div>
        </div>
    </div>
</div>