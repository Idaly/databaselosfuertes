<?php include 'includes/menu.php'; 
protect_page();

?>

<header>

<!--<div class="container">-->
<nav class="navbar navbar-inverse navbar-custom">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsingNavbarMd">  <!--data-target="#myNavbar"-->
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><img src="Logo_LosFuertes.png" class="img-rounded"  style="	width: 80px;"></a>
    </div>
           
		     <!-- Collect the nav links, forms, and other content for toggling -->
	 <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-left">
         	<li class="active"><a href="index.php" >Home </a> </li>      	
          
        
          <!--*********-->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <!--<div class="collapse navbar-collapse" id="myNavbar">-->
          
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Orders <span class="caret"></span></a>
              <ul  role="menu"  class="dropdown-menu">
              	<li><a href="FindOrder.php">Find Orders</a></li>
                <!--<li role="presentation"><a href="FindOrders.php" role="menuitem">Find Orders</a></li>-->
                <li><a href="ordersAdd.php">New Orders</a></li>
                <li><a href="addMissingPrices.php">Add Missing Prices</a></li>
                <li class="divider"></li>
                 <?php 
				$privilege =  $user_data['privilege'];
				if  (($privilege == "1") || ($privilege == "1")){
					echo '<li><a href="OrderReport.php">Orders Report (Only Managers)</a></li>';
				}
				?>
				
              </ul>
            </li>
         
        <!-- Collect the nav links, forms, and other content for toggling -->
		   
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Deliveries <b class="caret"></b></a>
              <ul class="dropdown-menu">
             	<li><a href="deliverySheet.php">Delivery Sheet</a></li>
                
              </ul>
         	</li>
          
          <!--*********-->
            
		<!-- Collect the nav links, forms, and other content for toggling -->
		<!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">-->
        
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products List <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="products.php">View all Product</a></li>
                <li class="divider"></li>
                 <?php 
				$privilege =  $user_data['privilege'];
				if  (($privilege == "1") || ($privilege == "1")){
					echo '<li><a href="productsAdd.php">Add New Products</a></li>';
				}
				?>
				
              </ul>
            </li>
          
          <!--*********-->
          
         <!-- Collect the nav links, forms, and other content for toggling -->
		<!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">-->
          
           <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Inventory <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="inventoryReport.php">Inventory Report</a></li>
                <li><a href="EntryInventory.php">Entry Inventory</a></li>
                
              </ul>
         </li> 
         <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vendors <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="AddVendor.php">Add Vendor</a></li>
                
              </ul>
         </li> 
        
          <!--*********-->
           <!-- Collect the nav links, forms, and other content for toggling -->
		
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Purchases Required<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="productsRequested.php">Products from Orders</a></li>
                <li><a href="#">Purchase Register</a></li>
                
              </ul>
            </li>
        
          <!--*********-->
       
         <!-- Collect the nav links, forms, and other content for toggling -->
		
          
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">   
           </button>
          	<li><a href="contact.php">Contact Us</a></li>
               
           
           
           
         
          <!--*********-->
             
                <!-- Collect the nav links, forms, and other content for toggling -->
		
		<!--<ul class="nav navbar-nav navbar-right">
			
         <!-- <ul class="nav navbar-nav">-->
          	
          	 <?php 
				$privilege =  $user_data['privilege'];
				if  (($privilege == "1") || ($privilege == "1")){
				?>
					<li><a href="register.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
				<?php
				}
				?>
         <!--</ul>-->
          	 
         	<li class="dropdown">
            	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome! <?php echo $user_data['FullName']; ?>!</a>
            	
            	<ul class="dropdown-menu">
	                <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Log Out</a></li>
					<!--<li><a href ="changepassword.php" >Change Password</a></li>
					<!--<li><a href ="settings.php" >Settings</a></li>-->
					<li>
					<!--this href also work it's more clear but the secon link below use a file  .htaccess
					<a href ="<?php echo $user_data['UserName']; ?>" >Profile</a>-->
					<!--	<a href="profile.php?UserName=<?php echo $user_data['UserName']; ?>">Profile</a>-->
					</li>
              	</ul>
            </li> 	
            </ul>	
          <!--</ul>-->
         
          </div>
        
        </div>
        <!-- /.navbar-collapse -->
        
      </div>
      <!-- /.container-fluid -->
    
	<!-- /</div>-->
 </nav>
 

  	<!--****************************-->

</header>