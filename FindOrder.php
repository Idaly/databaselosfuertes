<!DOCTYPE html>
	
<?php 
include 'core/init.php';
protect_page();

include 'includes/overall/header.php';

$current_url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
$_SESSION['userName']= $user_data['FullName'];
$_SESSION['userGUID']= $user_data['UserGUID'];
?>

 <head>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  
  <style>
  body
  {
   margin:0;
   padding:0;
   background-color:#f1f1f1;
 
  }
  .box
  {
   /*width:1270px;*/
   width:inherit;
   padding:7px;
   background-color:#fff;
   border:1px solid #ccc;
   border-radius:5px;
   margin-top:10px;
   box-sizing:border-box;
   
  }
  </style>
 </head>
 <body>
 	
  	
 <div class="container-fluid text-center">
 <div class="row content">
 <div class="col-sm-11 col-md-12 text-left">
 <form name="dateform" action="" method="POST">

  <!-- <div class="container box">-->
   <h1 align="center">Los Fuertes Orders List</h1>
   <br />	
 	
 <div class="container-fluid text-center">
 <div class="col-4">
 <FORM>
 <table >
 	<tr >
	 	<td >From:</td>
	 	<td >To:</td>
 	</tr>
 	<tr>
 		<td><input class='form-control' type='date' id = 'orderFrom' name='orderTo' min='2017-07-01'  Value= ''></td>  <!--'".$_SESSION['DateFrom']."'-->
		<td><input class='form-control' type='date' id = 'orderTo' name='orderTo' min='2017-07-01'  Value= ''></td>
		
	</tr>
	<tr></tr>
	<tr>
		<td><input class="btn btn-primary" type="button" name="findOrderBttn" id="findOrderBttn" Value="Find" ></td>
		<td><input class="btn btn-primary" type="button" name="button2" id="BttnClear"Value="Clear Date" ></td>
		<td><input class="btn btn-success" type="button" name="BttnRefresh" id="BttnRefresh"Value="Refresh" ></td>
	</tr>

</table>
</FORM>
</div>	
</div>	


<!-- /////////////////////////////////////////////////////////////////
	////////////this start the table data-->
   <div class="table-responsive">
   <br />
   <!-- <div align="right">
     <button type="button" name="add" id="add" class="btn btn-info">Add</button>
    </div>
    <br />-->
    <div id="alert_message"></div>
    <div ><input type="hidden" id="userId" name="userId" value="<?php echo $user_data['FullName']; ?>" />
    	  <input type="hidden" id="userGUID" name="userGUID" value="<?php echo $user_data['UserGUID']; ?>" /></div>
   
    <table id="user_data" class="table table-bordered table-striped">
     <thead>
      <tr  class="bg-primary">
       <th width="5%">Date</th>
       <th width="5%">Folio</th>
       <th width="10%" >CustomerName</th>
       <th width="5%">Routes</th>
       <th width="5%">DriverName</th>
       <th width="5%">Edit</th>
       <th width="10%">Status</th>
       <th width="5%">Print Order</th>
       
      </tr>
     </thead>
    </table>
   </div>
  </div>
  </div>
  </div>
  
 <!-- start table to show details and update  --> 
  <div class="container box">
   <h1 align="center">Orders Products List</h1>
   <br />
   <div class="table-responsive">
   <br />
    <div align="center">
   	 <button type="button" name="print2" id="print2" class="btn btn-success"><span class="glyphicon glyphicon-print"></span>  Print Order</button>
     <button type="button" name="add" id="add" class="btn btn-primary">Add New Product</button>
    </div>
    <br />
    <div id="alert_mssgDetails"></div>
    <div><input type= "hidden" id="userId" value=  /></div>
    <table id="detailsOrder_data" class="table table-bordered table-striped">
     <thead >
      <tr class="bg-primary">
       <th>CustomerName</th>
       <th>Out Stock</th>
       <th>Qty</th>
       <th>Item Description</th>
       <th>Comments</th>
       <th>Unit Price</th>
       <th>Lot No.</th>
       <th>Check/Verifier</th>
       <th>RemoveBttn</th>
      </tr>
     </thead>
    </table>
   </div>
  </div>
  
<!-- Endt table to show details and update  --> 
 
  
 </body>
 <!--</html>-->

<?php
include 'includes/overall/footer.php';
?>

<script type="text/javascript" language="javascript" >
 $(document).ready(function(){
 	
   $('#BttnRefresh').click(function () { 
  	$('#user_data').DataTable().destroy();
  	fetch_data();
   });
 	
  $('#BttnClear').click(function () { 
  	window.location.reload()
   });
   
  $('#findOrderBttn').click(function () { 
  	  $('#user_data').DataTable().destroy();
  	  $('#detailsOrder_data').DataTable().destroy();
  	  $( ".updateDetails" ).empty(); 
	  fetch_data();
   });
   
  function fetch_data()
  {
  	var dateFrom = document.getElementById('orderFrom').value;
  	var dateTo = document.getElementById('orderTo').value;
  	
  	if(dateFrom != '' && dateTo != '')
   	{  
	    var dataTable = $('#user_data').DataTable({
	    "processing" : true,
	    "serverSide" : true,
	    "order" : [],
	    "ajax" : {
	     url:"core/functions/fetch_FindOrder.php",
	     type:"POST",
    	 data:{dateFrom:dateFrom, dateTo:dateTo}
	    }
	   });
	 }else	{
    	 alert("Both Dates are required");
    	 $('#user_data').DataTable().destroy();
   	 }
  }
  
    $(document).on('click', '.printBttn', function(){	
		var id = $(this).data("id");
		PrintOrder(id);
    });
   $(document).on('click', '#print2', function(){	
	 	var id = this.value;
	 	
	  	PrintOrder(id);
   });
  
  function PrintOrder (id)
  {
		  	$.ajax({
		    url:"core/functions/scprtPrintPDF.php",
		    method:"POST",
		    dataType : 'json',   ///add this line para que pueda ver los valores de un array
		    data:{id:id},
		    success:function(data)
		    {
		      console.log(data);
		      var orderFolio = data.OrderFolio; 
		      var custGUID = data.CustGUID; 
		   //  $('#alert_message').html('<div class="alert alert-success">'+ data.OrderFolio+'</div>');    	//  
		       // $(".custName2").html(data.message2);                                            //window.location.assign(rdurl+"?sid="+sid);
		       window.open('printPDF.php?FolioNum=' + orderFolio + '&&custGUID='+custGUID);
		      // window.location = 'printPDF.php?FolioNum=' + orderFolio + '&&custGUID='+custGUID;
    		}
   			});
			setInterval(function(){
		   $('#alert_message').html('');
		   }, 10000);
  }
  
 /////edit button 
 
  $(document).on('click', '.editButtons', function(){	
  	var id = $(this).data("id");
    $('#detailsOrder_data').DataTable().destroy();
    EditBttn_data(id);
    $('html,body').animate({
		scrollTop: $("#detailsOrder_data").offset().top},
		'slow');
  });
  
  function EditBttn_data(id)
  {
  	if(id != '')
   	{  
   		document.getElementById("print2").value=id; //add id to print button 
   		document.getElementById("add").value=id;   //add id to button add
	    var dataTable = $('#detailsOrder_data').DataTable({
	    "processing" : true,
	    "serverSide" : true,
	    //"order" : [],
	    "ajax" : {
	     url:"core/functions/FindDetailsOrder.php",
	     type:"POST",
    	 data:{id:id}
    
	    }
	   });
	 }else	{
    	// alert("Both Dates are required");
    	 $('#detailsOrder_data').DataTable().destroy();
   	 }
   }
  /////////////////////////////
 function update_dataStock(id, column_name, value,extra1)
  {
	  	var userId = document.getElementById("userId").value; 
	  	var userGUID = document.getElementById("userGUID").value;
	  	var extra1;
	  //	alert (userGUID);
	    $.ajax({
	    url:"core/functions/UpdateOutStock.php",
	    method:"POST",
	   // dataType: "json",
	    data:{id:id, column_name:column_name, value:value,userId:userId,userGUID:userGUID,extra1:extra1},
	    success:function(data)
	    
	    {
	    	if (data == 'Data Updated'){
	    		//$('#alert_mssgDetails').html('<div class="alert alert-success">'+data+'</div>');
		    
			     $('#detailsOrder_data').DataTable().destroy();
			     EditBttn_data(id);
			     
			   //  $('#user_data').DataTable().destroy();
			   	 //$('#user_data').DataTable().ajax.reload();
			     // fetch_data();
			      
	    	}else{
	    		//$('#alert_mssgDetails').html('<div class="alert alert-danger">'+data+'</div>');
		         alert(data);
		          $('#detailsOrder_data').DataTable().ajax.reload();
			    // $('#detailsOrder_data').DataTable().destroy();
			    // EditBttn_data(id);
	    	}
		     
	    }
	    
	   });
	   setInterval(function(){
	    $('#alert_mssgDetails').html('');
	   }, 7000);
	  }
	
 
  function update_data(id, column_name, value,ReLoadMainTable)
  {
	  	var userId = document.getElementById("userId").value; 
	  	var userGUID = document.getElementById("userGUID").value;
	  	//alert (ReLoadMainTable);
	  //	alert (userGUID);
	    $.ajax({
	    url:"core/functions/UpdateDetailsOrder.php",
	    method:"POST",
	   // dataType: "json",
	    data:{id:id, column_name:column_name, value:value,userId:userId,userGUID:userGUID, ReLoadMainTable:ReLoadMainTable},
	   
	    success:function(data)
	    {
	    	if (data == 'Data Updated'){
	    		//$('#alert_mssgDetails').html('<div class="alert alert-success">'+data+'</div>');
		         $('#detailsOrder_data').DataTable().ajax.reload();
		       //  $('#detailsOrder_data').html(data);
			   //  $('#detailsOrder_data').DataTable().destroy();
			  //   EditBttn_data(id);
			     
			     
		//	if (ReLoadMainTable =='TRUE'){
		//		$('#user_data').DataTable().destroy();
		//	      fetch_data();
		//	}          
			     
	    	}else{
	    	//	$('#alert_mssgDetails').html('<div class="alert alert-danger">'+data+'</div>');
		   	 alert(data);
		   	  $('#detailsOrder_data').DataTable().ajax.reload();
			 //    $('#detailsOrder_data').DataTable().destroy();
		    // EditBttn_data(id);
	    	}
		     
	    }
	   });
	 //  setInterval(function(){
	 //   $('#alert_mssgDetails').html('');
	 //  }, 7000);
	  }
	
////////take value from inputs and update data/////////
	$(document).on('blur', '.updateQtyDetails', function(){
	   var id = $(this).data("id");
	   var value = this.value;
	   //var value = document.getElementById('UpdateQty').value;
	   var column_name = $(this).data("column");
	  // var value = $(this).text();
	   update_data(id, column_name, value);
	   
  	});
  	$(document).on('blur', '.updateCommDetails', function(){
	   var id = $(this).data("id");
	   var value = this.value;
	   var column_name = $(this).data("column");
	  // var value = $(this).text();   /// this line is used when only have <div> with values not for input text 
	   update_data(id, column_name, value);
	   
  	});
  	$(document).on('blur', '.updatePriceDetails', function(){
	   var id = $(this).data("id");
	   var value = this.value;
	   var column_name = $(this).data("column");
	  // var value = $(this).text();
	   update_data(id, column_name, value);
	   
  	});
  	$(document).on('blur', '.updateLotDetails', function(){
	   var id = $(this).data("id");
	   var value = this.value;
	   var column_name = $(this).data("column");
	  // var value = $(this).text();
	  var ReLoadMainTable ='TRUE';
	  
	   update_data(id, column_name, value, ReLoadMainTable);
	   
  	});
  	$(document).on('change', '.updateVerifDet', function(){
	   var id = $(this).data("id");
	  // var valueprev = document.getElementById('updateVerif'+$(this).val("id"));  
	   var column_name = $(this).data("column");
	  //alert(valueprev);
	       if(this.checked) {
     		this.value = 1;
     		var value = this.value;
 		 }else{
     		this.value = 0;
     		var value = this.value;
  		 } 
       var ReLoadMainTable ='TRUE';
       
      // alert(value);
	
	   update_data(id, column_name, value, ReLoadMainTable);
	   
  	});
///////update route and driver////////////////
	$(document).on('change', '.updateRoute', function(){
	   var id = $(this).data("id");
	   var value = this.value;
	   var column_name = $(this).data("column");
	  // var value = $(this).text();
	  	
		  	$.ajax({
		    url:"core/functions/scprtPrintPDF.php",
		    method:"POST",
		    dataType : 'json',   ///add this line para que pueda ver los valores de un array
		    data:{id:id},
		    success:function(data)
		    {
		      //console.log(data);
		      var orderFolio = data.OrderFolio; 
		      var custGUID = data.CustGUID; 
		      
		      update_data_Route(column_name, value,orderFolio,custGUID);
		      
    		}
   			});
	
  	 });
  	 	$(document).on('change', '.updateDriver', function(){
	   var id = $(this).data("id");
	   var value = this.value;
	   var column_name = $(this).data("column");
	  // var value = $(this).text();
	  //	 alert (value);
	  	 
		  	$.ajax({
		    url:"core/functions/scprtPrintPDF.php",
		    method:"POST",
		    dataType : 'json',   ///add this line para que pueda ver los valores de un array
		    data:{id:id},
		    success:function(data)
		    {
		     // console.log(data);
		     // alert(data);
		      var orderFolio = data.OrderFolio; 
		      var custGUID = data.CustGUID; 
		      update_data_Driver(column_name, value,orderFolio,custGUID);
		      
    		}
   			});
	
  	 });
  	 
  function update_data_Route(column_name, value,orderFolio,custGUID)
  {
	  	var userId = document.getElementById("userId").value; 
	  	var userGUID = document.getElementById("userGUID").value;
	  	
	  //	alert (userGUID);
	    $.ajax({
	    url:"core/functions/UpdateRouteOrder.php",
	    method:"POST",
	    data:{column_name:column_name, value:value,userId:userId,userGUID:userGUID,orderFolio:orderFolio,custGUID:custGUID},
	  
	    success:function(data)
	    {	    	 
	    	if (data == 'Data Updated'){
	    		//$('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
		        
			     $('#user_data').DataTable().destroy();
			      fetch_data();
	    	}else{
	    		//$('#alert_message').html('<div class="alert alert-danger">'+data+'</div>');
		    	  alert(data);
			     $('#user_data').DataTable().destroy();
			      fetch_data();
	    	}
		     
	    }
	   });
	   setInterval(function(){
	    $('#alert_message').html('');
	   }, 7000);
	  }
	
 function update_data_Driver(column_name, value,orderFolio,custGUID)
  {
	  	var userId = document.getElementById("userId").value; 
	  	var userGUID = document.getElementById("userGUID").value;
	  	//alert ("update driver");
	  //	alert (userGUID);
	   //alert(column_name);
	    $.ajax({
	    url:"core/functions/UpdateDriverOrder.php",
	    method:"POST",
	    data:{column_name:column_name, value:value,userId:userId,userGUID:userGUID,orderFolio:orderFolio,custGUID:custGUID},
	    success:function(data)
	    {	    	 
	    	if (data == 'Data Updated'){
	    		//$('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
		    
			     $('#user_data').DataTable().destroy();
			      fetch_data();
	    	}else{
	    		//$('#alert_message').html('<div class="alert alert-danger">'+data+'</div>');
		          alert(data);
			     $('#user_data').DataTable().destroy();
			      fetch_data();
	    	}
		     
	    }
	   });
	   setInterval(function(){
	    $('#alert_message').html('');
	   }, 7000);
	}

/////////////////////////////////////////////////////////////
  	//=======this update the checkbox out stock=======
  	$(document).on('change', '.UpdateQtyStockDet', function(){
	   var id = $(this).data("id");
	   //var valueprev = document.getElementById('UpdateQtyStock'+$(this).val("id"));  
	   var column_name = $(this).data("column");
	   
	     if(this.checked) {
     		//alert('checked');
     		this.value = 1;
     		var value = this.value;
     		var priceCero =0;
     		var extra1= "Out Stock";
     		extra1 = priceCero;
     		update_dataStock(id, column_name, value, extra1);
     		
 		 }else{
     		//alert('unchecked');
     		this.value = 0;
     		var value = this.value;
     		var extra1 ="";
     		update_dataStock(id, column_name, value,extra1);
  		 } //alert(valueprev);
	   	     
  	});
  	
  //	$(document).on('blur', '.updateVerifDet', function(){
//	   var id = $(this).data("id");
//	   var value = this.value;
//	   var column_name = $(this).data("column");
	  // var value = $(this).text();
	  
//	   update_data(id, column_name, value);
	   
 // 	});
  	
  
////////////////////////////////////////////////////////////////
$(document).ready(function(){
  var count = 1;
  var index=1;
  $('#add').click(function(){
 
  var count = count + 1;	
  var html_code = "<tr id='row"+count+"'>";
  
  
   html_code += "<td  contenteditable='false' class='index' >"+index+" </td>";
   html_code +="<td  contenteditable='false'  ></td>";
   html_code += "<td ><input class='item_qty' id='add_qty' name='item_qty' type= 'number' ></td>";
    		
   html_code += "<td ><input type='hidden' id= 'add_productGUID'><input class= 'add_productName' id= 'add_productName' list='browsersProduct' placeholder='Product Name' required >" +
   	"<datalist id='browsersProduct'>" +		
		<?php
		while($rows=mysql_fetch_assoc($productList)){
		?>
		"<option id='<?php echo $rows["productGuid"]; ?>' value='<?php echo $rows["description"]; ?>'> " +
		<?php
		}	
		?>
 	"</datalist>";
	   
   html_code += "<td ><input id='add_comments' type='text' ></td>";
   html_code += "<td ><input id='add_unitprice' name='item_unitprice' type= 'number' ></td>";
   html_code += "<td ><input id='add_lotNum' name='lotNum' type= 'number' ></td>";
   html_code += "<td><button type='button' name='insert'  data-row='row' id='insert' class='btn btn-success btn-md'>Insert</button></td>";
  // html_code += "<td><button type='button' name='remove' data-row='row' class='btn btn-danger btn-md remove'>Remove</button></td>";   
   html_code += "</tr>";  
   index++ ;
   $('#detailsOrder_data tbody').prepend(html_code);
  }); 
   
 }) ;
  
  $(document).on('click', '#insert', function(){
 	  var id  = document.getElementById("add").value; 
 	  //alert(id);
 	  $.ajax({
		    url:"core/functions/scprtPrintPDF.php",
		    method:"POST",
		    dataType : 'json',   ///add this line para que pueda ver los valores de un array
		    data:{id:id},
		    success:function(data)
		    {
		      console.log(data);
		      var orderFolio = data.OrderFolio; 
		      var custGUID = data.CustGUID; 
		      var custName = data.CustName; 
		      var OrderDate = data.OrderDate; 
		      var LstIndx = data.LastIndx;
		      LastIndx = LstIndx ;
		     
		  	  InsertNewItem(orderFolio,custGUID,id,custName,OrderDate, LastIndx);
    		}
   	 	});
  });
   
  function InsertNewItem (orderFolio,custGUID,id,custName,OrderDate, LastIndx){
  	   var add_qty 			= $('#add_qty').val();
  	   var add_productGUID 	= $('#add_productGUID').val();
	   var add_productName 	= $('#add_productName').val();
	   var add_comments 	= $('#add_comments').val();
	   var add_unitprice 	= $('#add_unitprice').val();
	   var add_lotNum 		= $('#add_lotNum').val();
	  
	   var userId 	= document.getElementById("userId").value; 
	   var userGUID = document.getElementById("userGUID").value;
	   	   
	   if(add_qty != '' && add_productName != '')
	   {
		    $.ajax({
		     url:"core/functions/InsertNewProduct.php",
		     method:"POST",
		     data:{LastIndx:LastIndx, add_productGUID:add_productGUID, OrderDate:OrderDate, userId:userId, userGUID:userGUID, id:id, orderFolio:orderFolio, 
		     	   custGUID:custGUID, custName:custName, add_qty:add_qty, add_productName:add_productName, add_comments:add_comments, 
		     	   add_unitprice:add_unitprice, add_lotNum:add_lotNum},
		     success:function(data)
		     {
		     	//alert(data);
		    //  $('#alert_mssgDetails').html('<div class="alert alert-success">'+data+'</div>');
		      $('#detailsOrder_data').DataTable().destroy();
		      	EditBttn_data(id);
		      
			   $('#user_data').DataTable().destroy();
			      fetch_data();
		      
		     }
	    	});
	    	//setInterval(function(){
		  //   $('#alert_mssgDetails').html('');
		  //  }, 5000);
	   }
  }
  
  
  $(document).on('click', '.deleteDetails', function(){
   var id = $(this).attr("id");
   if(confirm("Are you sure you want to remove this?"))
   {
    $.ajax({
     url:"core/functions/DeleteDetailsOrder.php",
     method:"POST",
     data:{id:id},
     success:function(data){
     	alert(data);
     // $('#alert_mssgDetails').html('<div class="alert alert-success">'+data+'</div>');
      $('#detailsOrder_data').DataTable().destroy();
      EditBttn_data(id);
      $('#user_data').DataTable().destroy();
	  fetch_data();
    
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 5000);
   }
  });
 });
 
 $(function() {
		$(document).on("change",'.add_productName',function() {
					   //alert(this.name);
		 var opt = $('option[value="'+$(this).val()+'"]');
		 var productGuidNum = (opt.length ? opt.attr('id') : 'NO PRODUCTGUID');
		 document.getElementById("add_productGUID").value = productGuidNum;   ///past the value to input text in format		   		    
					    //alert(opt.length ? opt.attr('id') : 'NO OPTION');
		if (productGuidNum == 'NO PRODUCTGUID') {		
			alert('Error: Product does not exist in the list, Please try again');
			document.getElementById("add_productName").value=""; 
		}	
		});	
 });
 
 //////////////////////////////////////////////////////
 
</script>

</html>
