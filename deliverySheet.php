<<!DOCTYPE html>>
	
<?php 
include 'core/init.php';
protect_page();

include 'includes/overall/header.php';

$current_url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
$_SESSION['userName']= $user_data['FullName'];
$_SESSION['userGUID']= $user_data['UserGUID'];
?>

 <head>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  
  <style>
  body
  {
   margin:0;
   padding:0;
   background-color:#f1f1f1;
 
  }
  .box
  {
   /*width:1270px;*/
   width:inherit;
   padding:7px;
   background-color:#fff;
   border:1px solid #ccc;
   border-radius:5px;
   margin-top:10px;
   box-sizing:border-box;
  }

  </style>
 </head>
 <body>
 	
  	
 <div class="container-fluid text-center">
 <div class="row content">
 <div class="col-sm-11 col-md-12 text-left">
 
 <form name="dateform" action="" method="POST">

  <!-- <div class="container box">-->
   <h1 align="center">Delivery Sheet</h1>
   <br />	
 	
 <div class="container-fluid text-center">
 <div class="col-4">
 <FORM>
 <table class="dates">
 	<tr >
	 	<td >From:</td>
	 	<td >To:</td>
 	</tr>
 	<tr>
 		<td><input class='form-control' type='date' id = 'orderFrom' name='orderTo' min='2017-07-01'  Value= ''></td>  <!--'".$_SESSION['DateFrom']."'-->
		<td><input class='form-control' type='date' id = 'orderTo' name='orderTo' min='2017-07-01'  Value= ''></td>
		
	</tr>
	<tr></tr>
	<tr>
		<td><input class="btn btn-primary" type="button" name="findOrderBttn" id="findOrderBttn" Value="Find" ></td>
		<td><input class="btn btn-primary" type="button" name="button2" id="BttnClear"Value="Clear Date" ></td>
		<td><button type="button" name="bttnPrint" id="bttnPrint"Value=""class="btn btn-success btn-md success" ><span class="glyphicon glyphicon-print"></span>Print Delivery Sheet</button></td>
	</tr>

</table>
</FORM>
</div>	
</div>	


<!-- /////////////////////////////////////////////////////////////////
	////////////this start the table data--> 
   <div class="table-responsive">
   <br />
   <!-- <div align="right">
     <button type="button" name="add" id="add" class="btn btn-info">Add</button>
    </div>
    <br />-->
    <div id="alert_message"></div>
    <div ><input type="hidden" id="userId" name="userId" value="<?php echo $user_data['FullName']; ?>" />
    	  <input type="hidden" id="userGUID" name="userGUID" value="<?php echo $user_data['UserGUID']; ?>" /></div>
   
    <table width="100%" id="delivery_data" border="1" rules="all" class="table table-bordered table-striped"> <!-- style="border:solid black; border-width:1px; border-color:black"class="table table-bordered table-striped"> <!--class="table table-bordered table-striped">-->
     <thead>
      <tr>
         <th colspan="7"><h3> Daily Delivery Sheet </h3></th>
         <th class="bg-primary" align="center" colspan="3">Status</th>
      </tr>
      <tr  class="bg-primary">
       <th width="2%">Date</th>
       <th>Driver Name</th>
       <th>Route</th>
       <th width="6%">Invoice #</th>
       <th>Customer Name</th>
       <th width="7%">Invoices Pending to pay</th>
       <th width="5%">Arrived Time </th>
       <th width="5%">Cash</th>
       <th width="5%">Check No.</th>
       <th width="5%">Pending</th>
       <th width="20%">Expenses or Comments</th>
      </tr>
     </thead>
    </table>
   </div>
  </div>
  </div>
  </div>

  
 </body>
 <!--</html>-->

<?php
include 'includes/overall/footer.php';
?>

<script type="text/javascript" language="javascript" >
 $(document).ready(function(){
 	
  $('#BttnClear').click(function () { 
  	window.location.reload()
   });
   
  $('#findOrderBttn').click(function () { 
  	  $('#delivery_data').DataTable().destroy();
  	  
	  fetch_data();
   });
  });
    
  function fetch_data()
  {
  	var dateFrom = document.getElementById('orderFrom').value;
  	var dateTo = document.getElementById('orderTo').value;
  	
  	if(dateFrom != '' && dateTo != '')
   	{  
	    var dataTable = $('#delivery_data').DataTable({
	    "processing" : true,
	    "serverSide" : true,
	    "order" : [],
	    "ajax" : {
	     url:"core/functions/fetch_DeliverySheet.php",
	     type:"POST",
    	 data:{dateFrom:dateFrom, dateTo:dateTo}
	    }
	   });
	 }else	{
    	 alert("Both Dates are required");
    	 $('#delivery_data').DataTable().destroy();
   	 }
   	}
 
 function printPage()
{
   var divToPrint=document.getElementById("delivery_data");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}  	

$('#bttnPrint').on('click',function(){
printPage();
})

</script>

</html>
