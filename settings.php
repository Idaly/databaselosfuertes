<?php 
include 'core/init.php';
protect_page();
include 'includes/overall/header.php';

if (empty($_POST) ===false ){
	$required_fields = array ('FullName','last_name','email');
	/// echo '<pre>', print_r($_POST, true), '</pre>';   //used to show the array with values
	foreach($_POST as $key=>$value){
		if (empty($value) && in_array($key, $required_fields) ===true ) {
			$errors[]='All Fields are required';
		Break 1;	
		}
	}
	if (empty($errors) === TRUE) {
		If(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === FALSE){
			$errors[]= 'A valid email address is required';
		// if email is valid fo into this step--this going to check if the email already exist in all table for any other user,
		// if email exist in table and any email in the table doesn't equal to  email from format
		
		}else if (email_exists($_POST['email']) === true && $user_data['email'] !== $_POST['email']) {   
			$errors[]= 'Sorry, the email \''.$_POST['email']. '\' is already in use'; // that email is used by other user.
		}
	}
	print_r($errors);
}

?>
<?php

if (isset($_GET['success']) === true && empty ($_GET['success']) === true) {
	echo "Your details have been updated!!";
}else { ///other ways
	// if there is not fiels empties and there is not errors 
	if (empty($_POST)=== false && empty($errors) === true) {
		//update user details  ---update_data is avariable create in this moment
		$update_data = array (
		'FullName'		=> $_POST['FullName'],
		//'last_name' 	=> $_POST['last_name'],
		'email' 		=> $_POST['email']
		//'allow_email'	=> ($_POST['allow_email'] =='on') ? 1 :0  // if allow_email box checked the value to update is 1 otherwise will be cero (no checked)
		
		);
		//print_r($update_data);  //show error in page i can use this for test...
		/// this line call a function and update data into sql
		
		update_user($session_user_id , $update_data);
		
		header('Location:settings.php?success');
		exit();
		
	}else if (empty($errors) === false) {
		echo output_errors($errors);
	}
	?>
		<h1>Settings</h1>
		
		<form action ="" method="post">
			<ul>
				<li>
					Full Name*:<br />
					<input type="text" name="FullName" value="<?php echo $user_data['FullName']; ?>">
				</li>
				
				<li>
					Email*:<br />
					<input type="text" name="email" value="<?php echo $user_data['email']; ?>">
				</li>
				<!--<li>
					Privilege*:<br />
					<select name="formprivilege">
	  					<option value="<?php echo $user_data['Privilege'];?>" >User</option>
	  					<option value="<?php echo $user_data['Privilege'];?>" >Supervisor</option>
	  				</select>
					 <input type="text" name="privilege" value="<?php echo $user_data['Privilege']; ?>">
				</li>-->
				<!--<li>
					Area*:<br />
					<input type="text" name="area" value="<?php echo $user_data['Area']; ?>">
				</li> -->
				<!--<li>
					<input type="checkbox" name= "allow_email" <?php if ($user_data['allow_email'] == 1) { echo 'checked ="checked"';} ?> >Would you like receive email from us?
				</li>-->
				
				<li>
					<input type="submit" value="Update"> 
				</li>
		
			</ul>
				
		</form>
				
	<?php
}  // close the curly bracket fron get success-- go top 
include 'includes/overall/footer.php';
?>

