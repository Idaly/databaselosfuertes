
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<?php 
include 'core/init.php';
//protect_page();
include 'includes/overall/header.php';

$current_url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
 
if (!isset($_SESSION["prdName"]) )//&& !empty($_SESSION["itmINdx"]))
{
	//reset when order has been submite again to zero
	$_SESSION["prdName"]="";
}
$_SESSION['userName']= $user_data['FullName'];
$_SESSION['userGUID']= $user_data['UserGUID'];

////////***************************************************************************////////////////////////
?>

<style>
.error {color: #FF0000;}
</style>

<!DOCTYPE html>
<html>

<body>

<div class="container">  <!--class="table-responsive"  -->
	<div class="row">
        <div class="col-md-6">
		   	<div id="alert_message"></div>
		    <table  id="crud_table">  <!--class="table table-bordered"-->
		    <form method="post" action=""> 
		    	<fieldset><legend class="text-center"> ADD NEW VENDOR <span class="req"><small> required *</small></span></legend> 
		    	
				 <tr><td> Name Vendor: </td><td><input type="text" id="name" class="" class="form-group" required>
				  	<span class="error">* </span></td></tr>
				  	 <br><br>
				
				 <tr><td> Address:</td><td><textarea id="address"rows="5" cols="40" ></textarea>
				  	<span class="error">* </span></td></tr>
				   <br><br>
				   
				 <tr><td>E-mail: </td><td><input type="text" id="email">
				  <span class="error"> </span>
				  <br><br>
				 <tr><td>Phone: </td><td><input type="text" id="phone">
				  <span class="error"></span>
				  <br><br>
				  <tr><td>Fax: </td><td><input type="text" id="fax">
				  <span class="error"></span>
				  <br><br>
				 <tr><td>Comment: </td><td><textarea id="comment" rows="5" cols="40"></textarea>
				  <br><br>
				  
				  	<div align="center">
		     		<button type="button" name="save" id="save" class="btn btn-success btn-xl">Save Vendor</button>
		    		</div>
			</form>	
		    </table>	
		    </div>
   
		    
		    
		    <div id="inserted_item_data"></div>
		 
  		</div>
	</div>
</div>
 </body>
</html>


<!-- Extra JavaScript/CSS added manually in "Settings" tab -->
<!-- Include jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">

//////save order////////////////////////////
		 $('#save').click(function()
		 {    	
			  document.getElementById("save").innerHTML="Processing, Please wait...";
			  $('#save').attr('disabled','disabled');
			  
			  var Name 		= document.getElementById("name").value;
			  var Address	= document.getElementById("address").value;
			  var Email		= document.getElementById("email").value;
			  var Phone		= document.getElementById("phone").value;
			  var Fax		= document.getElementById("fax").value;
			  var Comment	= document.getElementById("comment").value;
	
			  
			 $.ajax({
			   url:"InsertVendor.php",
			   method:"POST",
			   data:{Name:Name, Address:Address, Email:Email, Phone:Phone,Fax:Fax, Comment:Comment},
			   success:function(data){
			   	
			   		//alert(data);
			   		//console.log(data);
				    $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
					window.location.reload()	
				
			    }
			  });
			  
		  });
		    ////end clear all data   =======
		 

</script>
