<?php

//require ('fpdf/fpdf.php');
include_once ('core/functions/config.php');
require ('core/functions/fpdf/fpdf.php');

global $mysqli;	


$printFrom =$_GET['printFrom'];
$printTo =$_GET['printTo'];

    // Leer las líneas del fichero
   //muestra suma de todos los producto y commentarios de los mismos
  	$query=  'SELECT DATE(`OrderDate`) AS OrderDate1, `ProductName`,`Comments`, SUM(`Quantity`) as Quantity FROM `orders`
		
			WHERE `OrderDate` >= "' . $printFrom . '"  and `OrderDate` <=  "' . $printTo . '"  
			group by `ProductName`,`Comments` order by `OrderDate`, `ProductName` asc';  
  		
 	$result =$mysqli->query($query);
  	$obj = $result->fetch_object();
	$res = array(); 
	$res = $obj;
	
	if (!empty($obj)) {
		
		foreach ($result as $r) {
			$req[] = array('OrderDate1' => $r["OrderDate1"],'Comments' => $r["Comments"],'ProductName' => $r["ProductName"],'Quantity' => $r["Quantity"]);
			}
	
	}

class PDF extends FPDF
{
	
// Cargar los datos
function LoadData()
{


}	
	
// Cabecera de página
function Header()
{
    global $title;
	global $DateOrdered;
	
	//$folio =$_GET['FolioNum'];
	
	$wTitle =$this->GetStringWidth($title)+6;
	$this->SetX((210-$wTitle)/2);
    $this->Image('Logo_LosFuertes.png',10,12,30);
	
    // Arial bold 15
    $this->SetFont('Arial','B',18);
    $this->SetLineWidth(0.2);
    // Título
    $this->Cell($wTitle,10,$title,0,1,'R');
	
    	// Salto de línea
    $this->Ln(7);
	$this->SetFont('Arial','',10);
	$this->Cell(50,5,'7020 Troy Hill Drive Units H-J',0,0,'L',0);
	
	$this->Cell(60,5,'',0,0,'C');  
//	$this->Cell(40,5,'Date',1,0,'C');
//	$this->Cell(40,5,'Invoice #',1,0,'C');
	$this->Cell(40,5,'','',1,'L',0);   // empty cell with left,bottom, and right borders
	$this->Cell(50,5,'Elkridge, MD 21075',0,0,'L',0);
//	$this->Cell(60,5,'',0,0,'C'); 
//	$this->Cell(40,5,$DateOrdered,1,0,'C',0);
	//$this->Cell(40,5,$folio,1,0,'C',0);
	$this->Cell(40,5,'','',1,'L',0);   // empty cell with left,bottom, and right borders
	$this->Cell(50,5,'Phone:410-799-5571    Fax:410-799-5581',0,0,'L',0);
	
	// Salto de línea
    $this->Ln(8);
	
	$wbody = array(20, 55, 50, 25,20,20);   //total  185  set the column title
	
	$this->SetFont('Arial','B',10);
	$this->Cell($wbody[0],6,'Order Date',1,0,'C',0);  	//$this->Cell(40,5,'Words Here','LR',1,'C',0);  // cell with left and right borders
	//$this->Cell($wbody[1],10,$UserPrepered,1,0,'C',0);
	$this->Cell($wbody[1],6,'Product Name',1,0,'C',0);
	$this->Cell($wbody[2],6,'Comments',1,0,'C',0);
	$this->Cell($wbody[3],6,'Qty Ordered',1,'C');   // empty cell with left,bottom, and right borders
	//$this->Cell($wbody[3],6,'Qty Requested',1,0,'C',0);
	$this->Cell($wbody[4],6,'Inventory',1,'C');
	$this->Cell($wbody[5],6,'Qty Total',1,1,'C');
	//$this->Ln(3); 
		
}


	
function FancyTable($req) {
//	$Totalpdf="";
	
	$this->SetFont('Arial', '', 10);
	$wbody = array(20, 55, 50, 25,20,20);   //total  185  set the column title
	
	 foreach($req as $item)
            {
            				
            	$this->Cell($wbody[0],6,$item['OrderDate1'],1,0,'L');
				$this->Cell($wbody[1],6,$item['ProductName'],1,0,'L');
				$this->Cell($wbody[2],6,$item['Comments'],1,0,'L');
				$this->Cell($wbody[3],6,$item['Quantity'],1,0,'C');
				$this->Cell($wbody[4],6,"",1,0,'C');
				$this->Cell($wbody[5],6,"",1,1,'C');
				
				
				
			}
///this add row in blank			
$this->SetFont('Arial', 'B', 16);
for ($i=1; $i <=3 ; $i++) { 
	$this->Cell($wbody[0],7,'',1,0,'L');
	$this->Cell($wbody[1],7,'',1,0,'L');
	$this->Cell($wbody[2],7,'',1,0,'L');
	$this->Cell($wbody[3],7,'',1,0,'C');
	$this->Cell($wbody[4],7,'',1,0,'C');
	$this->Cell($wbody[5],7,'',1,1,'C');
}
	
//$this->SetX(110);  
//$this->Cell(35, 20,'Total',1,0,'R');	
//$this->Cell(50, 20,'$ '.$Totalpdf,1,1,'R');	

}

function Layout($req){
	$this->AddPage();
	$this->FancyTable($req);
}
// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',11);
    // Número de página
    $this->Cell(0,10,'Page '.$this->PageNo()."/{nb}",0,0,'R');
}

}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$title = 'Purchase Report';
$pdf->SetTitle($title);

//$pdf->AddPage();

	
	
//set default font/colors
$pdf->SetFont('Arial', '', 10);
 
$pdf->SetDrawColor(50,50,100);
//$pdf->AddPage();

$pdf->Layout($req);

$pdf->Output();

?>

