<?php 
include 'core/init.php';
//protect_page();
$current_url = base64_encode("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

if (empty($_POST) === false) {
	$required_fields =array('current_password', 'password','password_again');
	
	foreach($_POST as $key=>$value){
		if (empty($value) && in_array($key, $required_fields) ===true ) {
			$errors[]='All Fields are required';
			Break 1;
		}
	}
	// this cicle check if the current password is equal to sql data base 
	echo ($_POST['current_password']);
	echo "---";
	echo $user_data['password'];
	
	if (($_POST['current_password']) === $user_data['password']) {
		if (trim($_POST['password']) !== trim($_POST['password_again'])) {
			$errors[] = 'Your new password do not match';
			
		}else if (strlen($_POST['password']) < 6) {
			$errors[] = 'Your password must be at least 6 characters';
		}	
	}else {
		$errors[] = 'Your password is incorrect';
	}
	print_r($errors);
}

include 'includes/overall/header.php'; 

if (isset($_GET['success']) === true && empty($_GET['success']) === true) {
	echo "Your password has been changed.";
}else {
	if (isset($_GET['force']) === true && empty($_GET['force']) === true) {
	?>
	<p>You must change your password, now that you've requested.</p>
	<?php	
	} 
		
	if (empty($_POST) === false && empty($errors) === true) {
		// guardara informacion en tabla
			change_password($session_user_id, $_POST['password'] );  // este password corresponde al nuevo que quieren cambiar
			header ('Location:'. $return_url .'?success');  // success es para checar que el codigo funciona y guarda en mysql
						
	}else if (empty($errors) === false) {
			//output errors
			echo output_errors($errors);
	}
	
	?>
	
<div class="container-fluid text-center">    
  <div class="row content">
      <div class="col-sm-offset-1  col-sm-8 "> 
      
		<form class="form-horizontal" action="" method= "post">
			<legend class="text-center">Change Password</legend>
			<div class="form-group">
				<label class="control-label col-sm-3" for="CurrentPass">Current password*:</label>
				<div class="col-sm-5 col-md-offset-0">
					<input class="form-control" type="password" name="current_password">
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-sm-3" for="NewPass">New password*:</label>
				<div class="col-sm-5 col-md-offset-0">
					<input class="form-control" type="password" name="password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="NewPassagain">New password again*:</label>
				<div class="col-sm-5 col-md-offset-0">
					<input class="form-control" type="password" name="password_again">
				</div>
			</div>
			<div class="form-group">        
      			<div class="col-sm-offset-1 col-sm-8">
      				<input type='hidden' name='return_url' value='".$current_url."'/>";
			
					<input class="btn btn-lg btn-primary btn-block" type="submit" value="Change password" />	
				</div>
			</div>	
			
		</form>

    </div>
  </div>
</div>

<?php 
}  //aqui termina el else de get success
include 'includes/overall/footer.php'; ?>

