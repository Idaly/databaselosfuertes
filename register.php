
<?php
include 'core/init.php'; 
//this function don't show register page if is logged
//logged_in_redirect();
include 'includes/overall/header.php'; 

$current_url_NewUser = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

//////validation  /////////////////////
	if (empty($_POST) ===false ){
		$required_fields = array ('username','password','FullName');
	 ///echo '<pre>', print_r($_POST, true), '</pre>';   //used to show the array with values
		foreach($_POST as $key=>$value){
			if (empty($value) && in_array($key, $required_fields) ===true ) {
				$errors[]='All Fields are required';
			Break 1;	
		}
	}
		
	////check name////
	if (empty($error) === true) {
		if (user_exists($_POST['username']) ===true) {
			$errors[]='Sorry, the username \''.$_POST['username'].'\' is already taken.';
		}
		////se agrego 2 signos de == para que mostrara el error con 3 signos no lo muestra
		if (preg_match("/\\s/", $_POST['username']) ==true) {    
			$errors[]='Your username must not contain any spaces';
		}
		////check password
		if (strlen($_POST['password']) < 6) {
			$errors[]='Your password must be at least 6 characters.';
		}
		if (strlen($_POST['password']) > 32) {
			$errors[]='Your password is too long.';
		}
		if ($_POST['password']!== $_POST['password_again']) {
			$errors[] ='Your passwords do not match.';
		}
		if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)  {
			$errors[] ='A valid email address is required.';
		}
		If (email_exists ($_POST['email']) === true) {
			$errors[]='Sorry, the email \''.$_POST['email'].'\' is already in use.';
		} 
	}
}
//print_r($errors);  //show error in page ,can help to debug

?>

<?php

if(isset($_GET['success']) && empty ($_GET['success'])) {
	echo 'New User has been registed successfully!.';
	
	//header('Location:activate.php?email=idaly.reyes@gmail.com & email_code=752b7c160c64bc47b146e94f84e240e2');
}else {
		if (empty($_POST) === false && empty($errors) === true) {
			//register user   the left names are the titles from table in SQL 
			$UserGUID 		= uniqid();
			$IsActive      = "1";
			$register_data = array (
			'UserGUID'		=> $UserGUID,
			'username' 		=> $_POST['username'],
			'password'  	=> ($_POST['password']),
			'privilege'		=> $_POST['privilege'],
			//'password'  	=> md5($_POST['password']),
			'FullName'		=> ($_POST['FullName']),
			//'last_name' 	=> $_POST['last_name'],
			'email' 		=> $_POST['email'],
			'IsActive' 		=> $IsActive,
			'email_code'	=> ($_POST['username']+ microtime())
			//'email_code'	=> md5($_POST['username']+ microtime())
			);

			/// this line call a function and save data into sql
			register_user($register_data);
			
			header('Location:register.php?success');
			exit();
			
		}	else if (empty($errors) === false) {
			//output errors
			echo output_errors($errors);
		}
?>
<div class="container-fluid text-center">    
  <div class="row content">
      <div class="col-sm-8 col-md-offset-2"> 
      
		<form  class="form-horizontal" method="post" action="">
			<legend class="text-center">Register New User</legend>
			<div class="form-group">
				<label class="control-label col-sm-2" for="username">Username*: </label>
				<div class="col-sm-6">
					<input class="form-control" name="username" type="text" id="username" placeholder="Username" >
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-sm-2" for="password" >	Password*: </label>
				<div class="col-sm-6">
					<input class="form-control" name="password" type="password" id="password" placeholder="password" required>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="passwordagain">	Password again*: </label>
				<div class="col-sm-6">
					<input  class="form-control"name="password_again" type="password"  placeholder="Password again">
				</div>
			</div>
			<div class="form-group">	
				<label class="control-label col-sm-2" for="FullName">	FullName*:</label>
				<div class="col-sm-6">
					<input class="form-control" name="FullName" type="text"  placeholder="FullName">
				</div>
			</div>
			<div class="form-group">	
				<label class="control-label col-sm-2" for="Privilegelbl">Privilege:</label>
				<div class="col-sm-1">	
					  <select name="privilege" >
					    <option value="1">Manager</option>
					    <option value="2">Supervisor</option>
					    <option value="3">DataEntry</option>
				   
					  </select>
				</div>
			</div>
			<div class="form-group">		
				<label class="control-label col-sm-2" for="email">	Email*:</label>
				<div class="col-sm-6">
					<input class="form-control" name="email" type="email"   placeholder="Enter email">
				</div>
			</div>
			<div class="form-group"> 
				<div class="col-sm-offset-2 col-sm-8">
					<input type="submit" name="Submit" value="Register">
					<input type="reset" name="Submit2" value="Reset">
				</div>
			</div>
		</form>	
</div>
</div>
</div>
		
<?php 
}

include 'includes/overall/footer.php'; ?>