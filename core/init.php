<?php
session_set_cookie_params(0);
session_start(); // this is has to be a the top after php , no se actualizan las variables si no esta 
include_once ('config.php');

//error_reporting(0);

//require 'functions/config.php';
require 'database/connect.php';
require 'functions/general.php';
require 'functions/users.php';
require 'functions/searchData.php';
require 'functions/searchProduct.php';
require 'functions/scprtAddOrders.php';
require 'functions/scprtAddProduct.php';



$current_file = explode('/', $_SERVER['SCRIPT_NAME']);
$current_file = end($current_file);

if (logged_in()=== TRUE) {
	$session_user_id = $_SESSION['user_id'];
	$user_data = user_data($session_user_id,'UserGUID','user_id', 'privilege','username','password','FullName','email','email_code','IsActive','RecordCreated','RecordLastModified');	
	if (user_active($user_data['username']) === FALSE) {
		session_destroy();
		header('Location :logout.php');
		exit();
	}
	
	//$session_CustomerList = $_SESSION['customerGUID'];
	$customerList = readCustomerList('customerGUID','customerName','RecordCreated','RecordLastModified');
	
	$productList = readProductList('productGuid','description','recordCreated','recordLastModified');
	
	$vendorList = readVendorList('VendorName','vendorGUID','Address','Email','Phone','Fax','comments');

    $productPrice = GetProductPrice('price','productGuid','recordLastModified','description','qty');
	
	$productFromInventory = productFromInventory('TotalQty','SalesPrice','Price','productGuid','productName','recordLastModified');
	//$getFolio = GetOrderFolio();
	
}

//echo $user_data['active'];
$errors=array();

?>