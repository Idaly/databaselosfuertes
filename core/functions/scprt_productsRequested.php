<?php
session_set_cookie_params(0);
   session_start();
//include 'core/init.php';
include_once ('config.php');
require ('fpdf/fpdf.php');
$current_url = base64_encode("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

if (isset($_POST['prodRequestPDF'])) {

	$printFrom  =filter_var($_POST["printFrom"], FILTER_SANITIZE_STRING);
	$printTo    =filter_var($_POST["printTo"], FILTER_SANITIZE_STRING);

	print_r($printFrom);

	header("Location:../../ProdRequestedPDF.php?printFrom=" . $printFrom ."&printTo=".$printTo);
	
}


if (isset($_POST["btnSubmitReq"]) && $_POST["btnSubmitReq"] == 'ProductRequested' && isset($_POST["return_url"])) {   //&& isset($_POST["return_url"])) {
	// gets all the data from the form
	$from 		= filter_var($_POST["orderFrom"], FILTER_SANITIZE_STRING);
	$to 		= filter_var($_POST["orderTo"], FILTER_SANITIZE_STRING);
	 $return_url= base64_decode($_POST["return_url"]); //get return url
			
	$_SESSION["UserError"] = 0;
	$_SESSION['productReq']="";
	
	$_SESSION['dateFromProd']=$from;
	$_SESSION['dateToProd']=$to;
	
	findProd_Report($from, $to, $mysqli) ;
	
	print_r($return_url);
	header('Location:'.$return_url);	
	
}

function findProd_Report($from, $to, $mysqli) {

	$str = "";
	$dateOK = false;
	$dateOK = validateDate($from);
	if ($dateOK == FALSE) {
		$_SESSION["UserError"] = 41;
		//from date error
	}
	$dateOK = validateDate($to);
	if ($dateOK == FALSE) {
		$_SESSION["UserError"] = 42;
		//from date error
	}
	
	//$str = 'SELECT DATE(`OrderDate`) AS OrderDate1, `ProductName`,  Sum(`Quantity`) as Quantity , (inventory.Inv_Qty) as Inv_Qty, 
	//		(Sum(`Quantity`) - inventory.Inv_Qty) as totalPurchase  FROM `orders` 
	//		INNER JOIN inventory ON orders.ProductGUID = inventory.productGuid and inventory.recordLastModified >= "' . $from . '"
			
	//		WHERE `OrderDate` >= "' . $from . '"  and `OrderDate` <=  "' . $to . '"  
	//		group by `ProductName`order by `OrderDate`, `ProductName` asc';
			
			
		$str = 'SELECT DATE(`OrderDate`) AS OrderDate1, `ProductName`,`Comments`, SUM(`Quantity`) as Quantity FROM `orders`
		
			WHERE `OrderDate` >= "' . $from . '"  and `OrderDate` <=  "' . $to . '"  
			group by `ProductName`,`Comments` order by `OrderDate`, `ProductName` asc';
	
	$result =$mysqli->query($str);
	$obj = $result->fetch_object();
		  
	if (!empty($obj)) {
		
		foreach ($result as $r) {
			$prod_req[] = array('OrderDate1' => $r["OrderDate1"],'ProductName' => $r["ProductName"],'Comments' => $r["Comments"], 'Quantity' => $r["Quantity"]);
			}
	
			$_SESSION["productReq"] = $prod_req;
			$_SESSION['fieldsReq'] = "";	
			$_SESSION['status']= "prodOrdFound";
		//	print_r($prod_req);
			$_SESSION["orderlastReq"] = $str;	
	}	
			
		
			//print_r($prod_req);
				
}

	
/////==========Clear fields
if(isset($_POST["clearBttnReq"]))// && isset($_POST["return_url"]))	 
	{
	
		$_SESSION['productReq']="";
		$_SESSION['status'] = "";
		$_SESSION['fieldsReq'] = "";
		$_SESSION['dateFromProd']="";
		$_SESSION['dateToProd']="";
		
	   $return_url= base64_decode($_POST["return_url"]); //get return url
		header('Location:'.$return_url);
	}
		

function validateDate(&$dte) {//date by reference
	$test_arr = explode('/', $dte);
	if (count($test_arr) == 3) {
		if (checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
			$dte = $test_arr[2] . "/" . $test_arr[0] . "/" . $test_arr[1];
			//changes format comming from datepicker from MM/DD/YYYY to YYYY/MM/DD, otherwise will not be recognised by MySql
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

?>