<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php

if (session_status() == PHP_SESSION_NONE) {
  session_start();
  }
//include 'core/init.php'; 
include_once ('config.php');

$current_url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
   
if (!isset($_SESSION['CustItems']))
	{
		$_SESSION['CustItems']=array();
	}


if(isset($_POST["AddProduct"]) && isset($_POST["return_url"]))	 
		{
			//////validation  /////////////////////
			if ($_POST['datepicker'] == "" || $_POST['CustName'] == "" || $_POST['qty'] == "" || $_POST['productName'] == "") {
        	
	    		$return_url= base64_decode($_POST["return_url"]); //get return url
				//echo '<script>alert("All fields are required")</script>';
				$error='*All Fields are required';
				$_SESSION['error']=$error;
				header("Location:$return_url");
				
    		}else {
		       	 //echo "proceed...";
				// gets all the data from the form
				  $comments 	= filter_var($_POST["comments"], FILTER_SANITIZE_STRING); //product code
				  $createdDate 	= filter_var($_POST["datepicker"], FILTER_SANITIZE_STRING); //product code
				  $orderFolio  	= filter_var($_POST["orderFolio"], FILTER_SANITIZE_STRING); //product code
				  $CustName    	= filter_var($_POST["CustName"], FILTER_SANITIZE_STRING); //product code
				  $CustGUID    	= filter_var($_POST["custGuid"], FILTER_SANITIZE_STRING); //product code
				  $qty    		= filter_var($_POST["qty"], FILTER_SANITIZE_STRING); //product code
				  $prdName    	= filter_var($_POST["productName"], FILTER_SANITIZE_STRING); //product code
				  $GuidProduct  = filter_var($_POST["productGuid"], FILTER_SANITIZE_STRING); //product code
				  $price    	= filter_var($_POST["price"], FILTER_SANITIZE_STRING); //product code
				  $indx 		=  filter_var($_POST["idx"], FILTER_SANITIZE_STRING); //product code
				  //$total    = filter_var($_POST["total"], FILTER_SANITIZE_STRING); //product code
				 // $total    = $_POST['total']; 
				 $product = array();
				 $product['createdDate'] =$createdDate;
				 $product['orderFolio']= $orderFolio;
				 $product['CustName']= $CustName;
				 $product['custGuid']= $CustGUID;
				 $product['qty'] = $qty;
				 $product['prdName'] = $prdName;
				 $product['GuidProduct'] = $GuidProduct;
				 $product['comments'] = $comments;
				 $product['price'] = $price;
				 $product['indx'] = $indx;
				 
				
				 // create an array
				//$item=array($qty, $prdName, $price);		 
					// put the array in a session variable
				 $_SESSION['CustItems'][$indx] = $product;
				
				 $_SESSION['CustName'] = $CustName;
				 $_SESSION['CustGUID'] = $CustGUID;
				 $_SESSION['createdDate'] = $createdDate;
				 
				 $_SESSION['error']="";
				//print_r($indx); 
				 foreach ($_SESSION['CustItems'] as $value) {
				  	print_r ($value['prdName']);
				 }		
				//print_r($_SESSION['Items']); 
			     $return_url= base64_decode($_POST["return_url"]); //get return url
				 header("Location:$return_url");
			
	} 

}  // end (isset($_POST["AddProduct"])
	
	
/////===================Removes items for the list of the customer
	 if(isset($_POST["remove"]) )//&& isset($_POST["return_url"]))	 
	{
		  // gets all the data from the form	
		$indx    = filter_var($_POST["indx"], FILTER_SANITIZE_STRING); //product code	
		
		unset($_SESSION['CustItems'][$indx]);
		print_r($_SESSION['CustItems']);
		
	    $return_url= base64_decode($_POST["return_url"]); //get return url
		print_r($indx);
	    header('Location:'.$return_url);
	}
	
	
/////==========Clear fields
if(isset($_POST["clearOrderAdd"]))// && isset($_POST["return_url"]))	 
	{
	
		$_SESSION['CustItems']="";
		$_SESSION['CustName'] = "";
		$_SESSION['CustGUID'] = "";
		$_SESSION['createdDate'] ="";
		$_SESSION["itmIndx"] = 1; 
		
	   $return_url= base64_decode($_POST["return_url"]); //get return url
		header('Location:'.$return_url);
	}
		
	
////////======PLACE  order ========================================		
	 if(isset($_POST["submitOrder"]) )//&& isset($_POST["return_url"]))	 
	{
		
		    	$OrderGUID =uniqid (rand(), true);		
			foreach($_SESSION['CustItems'] as $row ){
				$place_Order = array (
				'OrderGUID'     => $OrderGUID,
				'OrderFolio'    => $row['orderFolio'],
				'OrderDate'     => $row['createdDate'],
				'CustomerName'  => $row['CustName'], 
				'Index'			=> $row['indx'],
				'CustomerGUID'  => $row['custGuid'],
				'ProductName'   => $row['prdName'],
				'ProductGUID'   => $row['GuidProduct'],
				'Comments' 		=> $row['comments'],
				'Quantity' 		=> $row['qty'],
				'UnitPrice' 	=> $row['price'],
				'UserGUID' 		=> $_SESSION['userGUID'],
				'UserName' 		=> $_SESSION['userName']
				);
				
			    //array_walk($place_Order,'array_sanitize');
				$fields = '`' .implode('`, `', array_keys($place_Order)) . '`';
				$data ='\'' . implode ('\', \'', $place_Order) . '\'';
				
				print_r($data);
				if($mysqli === false){
				   die("ERROR: Could not connect. " . mysqli_connect_error());
				}else{ 
				
				$mysqli->query("INSERT INTO `orders` ($fields) VALUES ($data);");
				echo '<script>alert("Order inserted successfully.!")</script>';
				$_SESSION['CustItems']="";
				$_SESSION['CustName'] = "";
				$_SESSION['createdDate'] = "";
				$_SESSION['error']="";
				}
				
				//echo"$sql";
				//echo "$mysqli";
			}
		
		    $return_url= base64_decode($_POST["return_url"]); //get return url
		    $_SESSION["itmIndx"]="1";   //THIS SESSION RETURN INDEX RESET TO NUM.1 EVERY NEW ORDER
		    header('Location:'.$return_url);
	}



?>