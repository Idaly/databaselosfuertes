<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>

<?php
session_set_cookie_params(0);
session_start();
//include 'core/init.php';
include_once ('config.php');
require ('fpdf/fpdf.php');
$current_url = base64_encode("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

if (!isset($_SESSION['viewDetailsOrders'])) {
	$_SESSION['viewDetailsOrders'] = array();
}
if (isset($_POST['print']) && isset($_POST["return_url"])) {

	$return_url = base64_decode($_POST["return_url"]);
	//get return url
	$orderFolioPrint ="";
	$orderFolioPrint = $_POST["ordFolio"];
	$custGUID= $_POST["CustomerGUID"];
	
	print_r($orderFolioPrint);
 
	//header("Location:../../printPDF.php?FolioNum=" . $orderFolioPrint."&&custGUID=".$custGUID);
	//header("Location:../../printPDF.php");
	header("Location:printPDF.php");

}

///////OrderReport find //////////////////////////////////////////////
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] == 'OrderReport' && isset($_POST["return_url"])) {
	// gets all the data from the form
	$from 			= filter_var($_POST["orderFrom"], FILTER_SANITIZE_STRING);
	$to 			= filter_var($_POST["orderTo"], FILTER_SANITIZE_STRING);
	$customerGUID 	= filter_var($_POST["customerGUID"], FILTER_SANITIZE_STRING);
	$customer 		= filter_var($_POST["orderCustomer"], FILTER_SANITIZE_STRING);
	$orderFolio 	= filter_var($_POST["orderFolio"], FILTER_SANITIZE_STRING);
	$opt_Report 	= filter_var($_POST["optReport"], FILTER_SANITIZE_STRING);
	
	$return_url = base64_decode($_POST["return_url"]);
	
	$_SESSION["UserError"] = 0;
	$_SESSION['OrderReport']="";
	//clears the error
	print_r($opt_Report);
	
	findOrders($from, $to, $customer, $customerGUID, $orderFolio, $opt_Report, $mysqli);

	$_SESSION['status'] = "OrderReportFound";
	print_r($_SESSION['status']);

	if (empty($_SESSION["OrderReport"])) {
		$_SESSION['fieldsReq'] = "NO DATA WAS FOUND!";
	}

	 //$_SESSION['DateFrom'] = $from;
	 //$_SESSION['DateTo'] = $to;
	 
	header('Location:' . $return_url);
}

///////findorder /////
//if(isset($_POST["findOrder"]) && $_POST["findOrder"]=='findOrder' && isset($_POST["return_url"]))
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] == 'findOrders' && isset($_POST["return_url"])) {
	// gets all the data from the form
	$from 			= filter_var($_POST["orderFrom"], FILTER_SANITIZE_STRING);
	$to 			= filter_var($_POST["orderTo"], FILTER_SANITIZE_STRING);
	$customerGUID 	= filter_var($_POST["customerGUID"], FILTER_SANITIZE_STRING);
	$customer 		= filter_var($_POST["orderCustomer"], FILTER_SANITIZE_STRING);
	$orderFolio 	= filter_var($_POST["orderFolio"], FILTER_SANITIZE_STRING);
	
	$return_url = base64_decode($_POST["return_url"]);
	//$_SESSION['viewDetailsOrders'] ="";

	$_SESSION["UserError"] = 0;
	$_SESSION['AllOrder']="";
	$opt_Report = "null";
	
	findOrders($from, $to, $customer, $customerGUID, $orderFolio, $opt_Report, $mysqli);

	$_SESSION['status'] = "FOUND";
	print_r($_SESSION['status']);

	if (empty($_SESSION["AllOrder"])) {
		$_SESSION['fieldsReq'] = "DATA WAS NOT FOUND" ;
		
		?> 
		<script type="text/javascript"> 
		alert("An error has occurred, data wasn\'t found !")</script>;
		
		<?php
		
	}
	 $_SESSION['DateFrom'] = $from;
	 $_SESSION['DateTo'] = $to;
	 
	header('Location:' . $return_url);
}

function findOrders($from, $to,$customer, $customerGUID, $orderFolio,  $opt_Report, $mysqli) {

	$_SESSION["AllOrder"] = array();

	$_SESSION["orderFrom"] = $from;
	$_SESSION["orderTo"] = $to;
	$_SESSION["orderCustomer"] = $customer;
	
	//$_SESSION["orderCustomer"] = $customerGUID;
	$_SESSION["orderFolio"] = $orderFolio;

	print_r($from);
	print_r($to);
	print_r($customer);
	print_r($customerGUID);
	
	
	//  $return_url= base64_decode($_POST["return_url"]); //get return url
	$str = "";
	$dateOK = false;
	$dateOK = validateDate($from);
	if ($dateOK == FALSE) {
		$_SESSION["UserError"] = 41;
		//from date error
	}
	$dateOK = validateDate($to);
	if ($dateOK == FALSE) {
		$_SESSION["UserError"] = 42;
		//from date error
	}

	if ($from <> "" && $to <> "" && $customerGUID == "" && $orderFolio == "") {
		if ($opt_Report == "OrderReport") { ///only manager can get this report
				$str = "SELECT  DATE(`OrderDate`) AS OrderDate1 ,  `CustomerGUID`,`OrderFolio`, `CustomerName` , `CheckVerified`, 
					`Comments`,`Quantity` , `ProductName` , `LotNum`, `UnitPrice`,(`Quantity`*`UnitPrice`) as total , `Route`,`DriverName`
					FROM `orders`  
			   		WHERE DATE(OrderDate) >=  '" . $from ."'  and DATE(OrderDate) <=  '" . $to . "' and IsDelivered  ='YES'
			   		order by `OrderFolio` asc ";
					
					$opt_Report == "null";
			
		}else  {   //user can geet this report
				$str = "SELECT SUM(IF(`CheckVerified`=0, 1, 0)) AS StatusOrder, SUM(IF(`LotNum`=0, 1, 0)) AS StatusLote, 
					count(`ProductName`) as TotalProd, DATE_FORMAT(orders.OrderDate, '%a %b %e %Y')AS OrderDate,  
					`CustomerGUID`,`OrderFolio`, `DriverName`, `Route`,
					`CustomerName` , `CheckVerified`, `Quantity` , `ProductName` , `LotNum`, `UnitPrice` 
					 FROM `orders`  
			   		WHERE DATE(OrderDate) >= '" . $from . "' and DATE(OrderDate) <= '" . $to . "' and IsDelivered  ='YES'
			   		group by `CustomerName` order by `Route` , `StatusOrder` asc ";
			   		//group by `OrderFolio` order by `Route`, `CustomerName` asc ";
		}
	} elseif ($orderFolio <> "" && $from == "" && $to == "" && $customerGUID == "") {
		$str = 'SELECT SUM(IF(`CheckVerified`=0, 1, 0)) AS StatusOrder, SUM(IF(`LotNum`=0, 1, 0)) AS StatusLote,
			count(`ProductName`) as TotalProd, DATE_FORMAT(orders.OrderDate, "%a %b %e %Y")AS OrderDate,  
			`CustomerGUID`,`OrderFolio`, `DriverName`, `Route`,
			`CustomerName` , `CheckVerified`, `Quantity` , `ProductName` , `LotNum`, `UnitPrice`
			FROM `orders` 
	   		WHERE OrderFolio = "' . $orderFolio . '" and IsDelivered  ="YES"
	   		 group by `CustomerGUID` order by `Route`,`StatusOrder` asc ';    ///ya ordena y muestra todos los folios existentes

	} elseif ($orderFolio == "" && $from == "" && $to == "" && $customerGUID <> "") {
		$str = 'SELECT SUM(IF(`CheckVerified`=0, 1, 0)) AS StatusOrder, SUM(IF(`LotNum`=0, 1, 0)) AS StatusLote,
			count(`ProductName`) as TotalProd, DATE_FORMAT(orders.OrderDate, "%a %b %e %Y")AS OrderDate,  
			`CustomerGUID`,`OrderFolio`, `DriverName`, `Route`,
			`CustomerName` , `CheckVerified`, `Quantity` , `ProductName` , `LotNum`, `UnitPrice`
			FROM `orders`  
	   		WHERE CustomerGUID = "' . $customerGUID . '" and IsDelivered  ="YES" 
	   		 group by `OrderFolio` order by `Route`,`StatusOrder` asc ';   ///show all folios and customers name

	} elseif ($orderFolio == "" && $from <> "" && $to <> "" && $customerGUID <> "") {
		$str = "SELECT SUM(IF(`CheckVerified`=0, 1, 0)) AS StatusOrder, SUM(IF(`LotNum`=0, 1, 0)) AS StatusLote,
			count(`ProductName`) as TotalProd, DATE_FORMAT(orders.OrderDate, '%a %b %e %Y')AS OrderDate,  
			`CustomerGUID`,`OrderFolio`, `DriverName`, `Route`,
			`CustomerName` , `CheckVerified`, `Quantity` , `ProductName` , `LotNum`, `UnitPrice`
			FROM `orders` 
	   		WHERE CustomerGUID = '" . $customerGUID . "' and DATE(OrderDate) >= '" . $from . "' and 
	   		DATE(OrderDate) <= '" . $to . "' and IsDelivered  ='YES'
	   		group by `OrderFolio` order by `OrderDate`,`OrderFolio` , `Route`,`StatusOrder`asc ";
	   		
	///this query id for get info after uodate
	} elseif ($orderFolio <> "" && $from <> "" && $to <> "" && $customerGUID == "") {
		$str = "SELECT SUM(IF(`CheckVerified`=0, 1, 0)) AS StatusOrder, SUM(IF(`LotNum`=0, 1, 0)) AS StatusLote,
			count(`ProductName`) as TotalProd, DATE_FORMAT(orders.OrderDate, '%a %b %e %Y')AS OrderDate,  
			`CustomerGUID`,`OrderFolio`,  `DriverName`, `Route`,
			`CustomerName` , `CheckVerified`,`Quantity` , `ProductName` , `LotNum`, `UnitPrice`
			FROM `orders` 
	   		WHERE DATE(OrderDate) >=  '" . $from ."'  and DATE(OrderDate) <=  '" . $to . "' and OrderFolio = '" . $orderFolio . "' and IsDelivered  ='YES'
	   		 group by `CustomerGUID` order by `Route`,`StatusOrder` asc  ";
	   		
	} else {
		$_SESSION['fieldsReq']= 1;
		
	}
print_r($orderFolio);
	$result = $mysqli -> query($str);
	$obj = $result -> fetch_object();
	
	print_r($orderFolio);
	print_r($result);
	
		//sucesfully added
/////CONDITION FOR ORDER REPORT OR FIND ORDER BY GROUP//////////////////////////////

	if (!empty($obj)) {
		if ($opt_Report == "OrderReport") {
			foreach ($result as $r) {
				$req[] = array('OrderDate1' => $r["OrderDate1"],'CustomerName' => $r["CustomerName"], 'Quantity' => $r["Quantity"], 
				'ProductName' => $r['ProductName'], 'LotNum' => $r['LotNum'], 'DriverName' => $r['DriverName'],'Route' => $r['Route'],
				'Comments' => $r["Comments"], 'UnitPrice' => $r["UnitPrice"], 'total' => $r["total"], 
				'OrderFolio' => $r["OrderFolio"], 'CustomerGUID' => $r['CustomerGUID'],'CheckVerified' => $r['CheckVerified']);
			}
			$_SESSION["OrderReport"] = $req;
			$_SESSION['fieldsReq'] = 0 ;	
			//print_r("hellodata");
			$_SESSION["orderlastReq"] = $str;	
		
		}else {	
			foreach ($result as $r) {
				$req[] = array('OrderDate' => $r["OrderDate"],'CustomerName' => $r["CustomerName"], 'Quantity' => $r["Quantity"], 'ProductName' => $r["ProductName"], 
				'LotNum' => $r["LotNum"], 'UnitPrice' => $r["UnitPrice"], 'OrderFolio' => $r["OrderFolio"], 'CustomerName' => $r['CustomerName'], 'Route' => $r['Route'], 
				'DriverName' => $r['DriverName'], 'CustomerGUID' => $r['CustomerGUID'],'CheckVerified' => $r['CheckVerified'],'StatusOrder' => $r['StatusOrder'], 'TotalProd' => $r['TotalProd'], 
				'StatusLote' => $r['StatusLote']);
			}
			$_SESSION["AllOrder"] = $req;
			$_SESSION['fieldsReq'] = 0;	
			//print_r("hellodata");
			$_SESSION["orderlastReq"] = $str;	
		}
////END  CONDITION FOR OREDER REPORT OR FIND ORDER BY GROUP///////////////
	
	
	}
}   // end funtion
/////======================VIEW DETAILS===================================================================================
//print_r($_POST["viewDetailsBtton"]);

if (isset($_POST["viewDetailsBtton"]) && $_POST["viewDetailsBtton"] == 'viewDetails' && isset($_POST["return_url"])) {
	print_r("view details function");
	//$_SESSION["viewDetailsOrders"]=array();

	// gets all the data from the form
	$customerName = filter_var($_SESSION["custName"], FILTER_SANITIZE_STRING);
	$customerGUID = filter_var($_POST["CustomerGUID"], FILTER_SANITIZE_STRING);
	$orderFolio = filter_var($_POST["ordFolio"], FILTER_SANITIZE_STRING);
	
	$return_url = base64_decode($_POST["return_url"]);
	//get return url	
	print_r($orderFolio);
	
	viewDetailsOrders($orderFolio, $customerGUID, $mysqli);
		
	header('Location:'.$return_url.'#back_details');
	die();
}

function viewDetailsOrders($orderFolio, $customerGUID, $mysqli) 
{
	
	$_SESSION['viewDetailsOrders'] = array();
	$str = 'SELECT *, Quantity*UnitPrice as Total
		  FROM `orders` 
		  WHERE OrderFolio = "' . $orderFolio . '" and CustomerGUID  = "' . $customerGUID . '" and IsDelivered  ="YES"';

	$result = $mysqli -> query($str);
	$obj = $result -> fetch_object();

		if (!empty($obj)) {
			foreach ($result as $r) {
					$details[] = array('Indexprod' => $r["Indexprod"],'OrderDate' => $r["OrderDate"], 'OrderFolio' => $r["OrderFolio"], 'CustomerGUID' => $r['CustomerGUID'], 
					'CustomerName' => $r['CustomerName'], 'ProductGUID' => $r['ProductGUID'], 'ProductName' => $r['ProductName'], 'DriverName' => $r['DriverName'],
					'Comments' => $r['Comments'], 'Quantity' => $r['Quantity'], 'LotNum' => $r['LotNum'], 'CheckVerified' => $r['CheckVerified'], 
					'UnitPrice' => $r['UnitPrice'], 'Total' => $r['Total'],'DriverName' => $r['DriverName'],'QtyOutStock' => $r['QtyOutStock']);	
			}
	
			$_SESSION["viewDetailsOrders"] = $details;
			
			$_SESSION['statusVIEW'] = "VIEW";
			$_SESSION['status'] = "FOUND";
			//print_r($details);
			
		
		}
		
}

/////=================clear data items for the list of the customer
if (isset($_POST["clearBttn"]) && $_POST["clearBttn"] == 'clearBttn' && isset($_POST["return_url"])) {
	cleardata();
}

function cleardata (){
	$_SESSION["viewDetailsOrders"] = "";
	//print_r('hello');
	$_SESSION["AllOrder"] = "";
	$_SESSION['statusVIEW'] = "";

	$_SESSION['status'] = "";
	$_SESSION['fieldsReq'] = "";
	$_SESSION["orderFolio"]="";
	$_SESSION['DateTo']="";
	$_SESSION['DateFrom']="";
	$return_url = base64_decode($_POST["return_url"]);
	//get return url
	header('Location:' . $return_url);
}

/////=======================add driver ========================================

if (isset($_POST["UpdateDriver"]) && $_POST["UpdateDriver"] == 'UpdateDriver' && isset($_POST["return_url"])) {
	
	$driverName		= filter_var($_POST["driverName"], FILTER_SANITIZE_STRING);
	$customerGUID 	= filter_var($_POST["customerGUIDAdd"], FILTER_SANITIZE_STRING);
	//$customer 		= filter_var($_POST["customerName"], FILTER_SANITIZE_STRING);
	$Folio 			= filter_var($_POST["orderFolioAdd"], FILTER_SANITIZE_STRING);
	$return_url 	= base64_decode($_POST["return_url"]);
	
	$str = 'UPDATE `orders` SET `DriverName`="' . $driverName . '"
	WHERE  `OrderFolio`="'.$Folio.'" and `CustomerGUID` = "'.$customerGUID.'" and IsDelivered  ="YES"';
	
	$result = $mysqli -> query($str);

	//print_r($driverName);
//	print_r($customerGUID);
	//print_r($Folio);
	$_SESSION['status']= "FOUND";
	$_SESSION['statusVIEW'] = "VIEW";
	
	//after update need to get the new data from sql so, we call the function again
	viewDetailsOrders($Folio, $customerGUID, $mysqli);
	
	header('Location:' . $return_url.'#back_details');
}

/////====================Update Route ========================================

if (isset($_POST["updateRoute"]) && $_POST["updateRoute"] == 'UpdateRoute' && isset($_POST["return_url"])) {
	print_r("update route");	
	$route			= filter_var($_POST["Routes"], FILTER_SANITIZE_STRING);
	$customerGUID 	= filter_var($_POST["CustomerGUID"], FILTER_SANITIZE_STRING);
	//$customer 		= filter_var($_POST["customerName"], FILTER_SANITIZE_STRING);
	$Folio 			= filter_var($_POST["ordFolio"], FILTER_SANITIZE_STRING);
	
	$from 			= filter_var($_POST["dateFromRef"], FILTER_SANITIZE_STRING);
	$to 			= filter_var($_POST["dateToRef"], FILTER_SANITIZE_STRING);
	
	$return_url 	= base64_decode($_POST["return_url"]);
	
	print_r($route);
	print_r($customerGUID);
	print_r($Folio);
	$str = 'UPDATE `orders` SET `Route`="' . $route . '"
	WHERE  `OrderFolio`="'.$Folio.'" and `CustomerGUID` = "'.$customerGUID.'" and IsDelivered  ="YES"';
	
	$result = $mysqli -> query($str);
	
	print_r($route);
    $_SESSION['status']= "FOUND";
	
	//call find orders after update 
	$customerGUID	="";
	$Folio 			="";
	$opt_Report 	= null;
	$customer  ="";
	findOrders($from, $to, $customer, $customerGUID, $Folio, $opt_Report, $mysqli);
	
	header('Location:' . $return_url);
}



/////========================UPDATE order ========================================

if (isset($_POST["update"]) && $_POST["update"] == 'submitUpdate' && isset($_POST["return_url"])) {
	print_r("UPDATE order");
	$from 			= filter_var($_POST["dateFrom"], FILTER_SANITIZE_STRING);
	$to 			= filter_var($_POST["dateTo"], FILTER_SANITIZE_STRING);
	$indxtable 		= filter_var($_POST["indx"], FILTER_SANITIZE_STRING);
	$customerGUID 	= filter_var($_POST["customerGUID"], FILTER_SANITIZE_STRING);
	$customer 		= filter_var($_POST["customerName"], FILTER_SANITIZE_STRING);
	$Folio 			= filter_var($_POST["orderFolio"], FILTER_SANITIZE_STRING);
	$productGUID 	= filter_var($_POST["productGUID"], FILTER_SANITIZE_STRING);
	$Qty 			= filter_var($_POST["orderQty"], FILTER_SANITIZE_STRING);
	$price 			= filter_var($_POST["unitPrice"], FILTER_SANITIZE_STRING);
	
	$checked = ( isset($_POST['check'])) ? 1 : 0;
	$checkedOutStock = ( isset($_POST['checkOutStock'])) ? 1 : 0;
	
	if ($checkedOutStock == 1){   // 1 means out stock
		$lotNum = "1";
		$comments ="Out Stock";
		
	}elseif ($checkedOutStock == 0 && $comments ="Out Stock"){
		$lotNum = filter_var($_POST["lotNum"], FILTER_SANITIZE_STRING);
		$comments 		= filter_var($_POST["comments"], FILTER_SANITIZE_STRING);
		//$checked ="0";	
	}
	
		
	
	$return_url = base64_decode($_POST["return_url"]);
	//get return url
	print_r($checked);
	
	//if ($lotNum > 0) {
	//	$str = 'UPDATE `orders` SET `LotNum`="' . $lotNum . '",`Quantity`="' . $Qty . '",`UnitPrice`="' . $price . '",`Comments`="' . $comments . '", 
	//	`CheckVerified`="' . $checked. '" , `LotUsedGUID`="'.$_SESSION['userGUID'].'", `UpdatedLotUser` ="'.$_SESSION['userName'].'"
	//	WHERE  `OrderFolio`="'.$Folio.'" and `CustomerGUID` = "'.$customerGUID.'" and `Indexprod`="'.$indxtable.'" and `ProductGUID` = "'. $productGUID.'"';
	
	//}else {
	
	//if ( $checked == 1 || $checked == 0) {   ///means that 
	//	$str = 'UPDATE `orders` SET `LotNum`="' . $lotNum . '",`Quantity`="' . $Qty . '",`UnitPrice`="' . $price . '",`Comments`="' . $comments . '", 
	//	`CheckVerified`="' . $checked. '" , `VerUsedGUID`="'.$_SESSION['userGUID'].'", `UpdatedVerUser` ="'.$_SESSION['userName'].'"
	//	WHERE  `OrderFolio`="'.$Folio.'" and `CustomerGUID` = "'.$customerGUID.'" and `Indexprod`="'.$indxtable.'" and `ProductGUID` = "'. $productGUID.'"';
	//}else{
		$str = 'UPDATE `orders` SET `LotNum`="' . $lotNum . '",`Quantity`="' . $Qty . '",`QtyOutStock`="' . $checkedOutStock . '", `UnitPrice`="' . $price . '",`Comments`="' . $comments . '", 
		`CheckVerified`="' . $checked. '" , `UpdatedGUID`="'.$_SESSION['userGUID'].'", `UpdatedUser` ="'.$_SESSION['userName'].'"
		WHERE  `OrderFolio`="'.$Folio.'" and `CustomerGUID` = "'.$customerGUID.'" and `Indexprod`="'.$indxtable.'" and `ProductGUID` = "'. $productGUID.'"';
	//}
	
//	}
	//WHERE `ProductGUID` = "' . $productGUID . '" and `CustomerGUID` = "' . $customerGUID . '"';
	//$result = $mysqli -> query($str);
	//print_r($str);

	if ($mysqli -> query($str) === TRUE) {
		?>
    	 <script>  alert("Product has been updated successfully") ; </script>;
    	 
    	<?php 
	} else {
	    echo "Error: " . $str . "<br>" . $mysqli->error;
		
		
	    header("Location: $return_url");
	}
	
	
	$_SESSION['status'] = "FOUND";
	$_SESSION['statusVIEW'] = "VIEW";

	//after update need to get the new data from sql so, we call the function again
	viewDetailsOrders($Folio, $customerGUID, $mysqli);

	$_SESSION['option'] = "Updated";
	-$_SESSION['index'] = $indxtable;

	print_r($comments);
	  header('Location:' . $return_url.'#back_details');
	

}

/////============CLEAR data items for the list of the customer=================================================
if (isset($_POST["done"]) && $_POST["done"] == 'submitDone' && isset($_POST["return_url"])) {
	$_SESSION["viewDetailsOrders"] = "";
	//print_r('hello');

	$return_url = base64_decode($_POST["return_url"]);
	//get return url
	header('Location:' . $return_url);
}

////////==========REMOVE Item from when is doing the verification ========================================

if (isset($_POST["remove"]) && $_POST["remove"] == 'RemoveItem' && isset($_POST["return_url"])) {
		
	$customerGUID 	= filter_var($_POST["customerGUID"], FILTER_SANITIZE_STRING);
	$orderFolio 	= filter_var($_POST["orderFolio"], FILTER_SANITIZE_STRING);
	$productGUID 	= filter_var($_POST["productGUID"], FILTER_SANITIZE_STRING);
	$Qty 			= filter_var($_POST["orderQty"], FILTER_SANITIZE_STRING);
	$index 			= filter_var($_POST["indx"], FILTER_SANITIZE_STRING);
	
	
	$str = 'UPDATE `orders` SET `IsDelivered`="NO" , `UpdatedGUID`="'.$_SESSION['userGUID'].'", `UpdatedUser` ="'.$_SESSION['userName'].'"
			WHERE `OrderFolio`="'.$orderFolio.'" and `CustomerGUID` = "'.$customerGUID.'" and `Indexprod`="'.$index.'" and `ProductGUID` = "'. $productGUID.'"';
			//WHERE `ProductGUID` = "'. $productGUID.'" and `OrderFolio`="'.$orderFolio.'" and `CustomerGUID` = "'.$customerGUID.'" and 
			//`Quantity`="'.$Qty.'" and `Index`="'.$index.'"';
	
	$result = $mysqli -> query($str);

	viewDetailsOrders($orderFolio, $customerGUID, $mysqli);
	print_r($index);
	
	$return_url = base64_decode($_POST["return_url"]);
	header('Location:' . $return_url.'#back_details');
}

////////////ADD iTEM ////////////////////////////

if (isset($_POST["addItem"]) && $_POST["addItem"] == 'AddItem' && isset($_POST["return_url"])) {
	print_r('add item');
	
	
	print_r("Add item function");
	$indxtable 	= filter_var($_POST["indx"], FILTER_SANITIZE_STRING);
	$orderDate 		= filter_var($_POST["orderDate"], FILTER_SANITIZE_STRING);
	$customerGUID 	= filter_var($_POST["customerGUIDAdd"], FILTER_SANITIZE_STRING);
	$customerName	= $_POST["customerNameAdd"];
	$Folio 			= filter_var($_POST["orderFolioAdd"], FILTER_SANITIZE_STRING);
	$productGUID	= filter_var($_POST["productGUIDAdd"], FILTER_SANITIZE_STRING);
	$productName 	= filter_var($_POST["productNameAdd"], FILTER_SANITIZE_STRING);
	$Qty 			= filter_var($_POST["orderQtyAdd"], FILTER_SANITIZE_STRING);
	$price 			= filter_var($_POST["unitPriceAdd"], FILTER_SANITIZE_STRING);
	$lotNum 		= filter_var($_POST["lotNumAdd"], FILTER_SANITIZE_STRING);
	$comments 		= filter_var($_POST["commentsAdd"], FILTER_SANITIZE_STRING);
	$userinsert		= $_SESSION['userName'];
	$dateinserted	= date('m/d/Y h:i:s', time());
	$checked 		= ( isset($_POST['checkAdd'])) ? 1 : 0;
	
	$checkedOutStock = ( isset($_POST['checkOutStock'])) ? 1 : 0;
	
	$return_url 	= base64_decode($_POST["return_url"]);
	
	print_r($_POST["customerNameAdd"]);

	$str='INSERT INTO `orders` ( `Indexprod`, `OrderFolio`, `CustomerGUID`,`CustomerName`,`ProductGUID`, `ProductName`,  `Quantity`, 
	`Comments`, `LotNum`, `CheckVerified`,`QtyOutStock`, `UnitPrice`, `OrderDate`, `RecordLastModified`, `UserGUID`, `UserName`) 
	VALUES ("'.$indxtable.'","'.$Folio.'", "'.$customerGUID.'", "'.$customerName.'", "'.$productGUID.'" , "'.$productName.'" , "'.$Qty.'" , 
	"'.$comments.'","'.$lotNum.'", "'.$checked.'",  "'.$checkedOutStock.'" ,"'.$price.'", "'.$orderDate.'", "'.$dateinserted.'",
	"'.$_SESSION['userGUID'].'", "'.$userinsert.'");';
	
	//$results = $mysqli->query($str);
	
	if ($mysqli->query($str) === TRUE) {
		?>
    	 <script> alert( "New Record created successfully")  </script>;
    	 <?php
	} else {
	    echo "Error: " . $str . "<br>" . $mysqli->error;
	    header("Location: $return_url");
	}
		
	print_r($str);
	
	//after update need to get the new data from sql so, we call the function again
	viewDetailsOrders($Folio, $customerGUID, $mysqli);

	$_SESSION['index'] = $indxtable;
	
	// Close connection
	//mysqli_close($mysqli);
    header('Location:' . $return_url.'#back_details');

	}
//}

//validate dates
function validateDate(&$dte) {//date by reference
	$test_arr = explode('/', $dte);
	if (count($test_arr) == 3) {
		if (checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
			$dte = $test_arr[2] . "/" . $test_arr[0] . "/" . $test_arr[1];
			//changes format comming from datepicker from MM/DD/YYYY to YYYY/MM/DD, otherwise will not be recognised by MySql
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}


?>




