<?php

//require ('fpdf/fpdf.php');
include_once ('core/functions/config.php');
require ('core/functions/fpdf/fpdf.php');

global $mysqli;	

//$folio = '2';
//$custGUID = 'f6fbe362-a983-408c-b582-22dfcfcd6e24' ;
$folio =$_GET['FolioNum'];
$custGUID =$_GET['custGUID'];


    // Leer las líneas del fichero
  	//$query=  'SELECT  * , DATE_FORMAT(orders.OrderDate, "%a %b %e %Y")AS OrderDate, Quantity*UnitPrice as Total FROM `orders` 
  	$query=  'SELECT orders.*, DATE_FORMAT(orders.OrderDate, "%a %b %e %Y")AS OrderDate, Quantity*UnitPrice as Total,
  		customerslist.shipaddress1,customerslist.shipaddress2,customerslist.phone, customerslist.fax
  		FROM `orders` INNER JOIN customerslist ON orders.CustomerGUID = customerslist.customerGUID
  		WHERE OrderFolio="'.$folio.'"  and IsDelivered  ="YES" and orders.CustomerGUID ="'.$custGUID.'" '; //OrderFolio="'.$folio.'" 
  		
 	$result =$mysqli->query($query);
  	$obj = $result->fetch_object();
	$res = array(); 
	$res = $obj;
	
	//DATE_FORMAT(orders.OrderDate, %a %b %e %Y)OrderDate,
	 if (!empty($obj)){
		  	foreach($result as $r) {
		  		$req[] = array('Quantity'=>$r["Quantity"],'LotNum'=>$r["LotNum"], 'ProductName'=>$r["ProductName"], 'Comments'=>$r["Comments"], 'UnitPrice'=>$r["UnitPrice"], 
				'CustomerName'=>$r["CustomerName"],'OrderFolio'=>$r["OrderFolio"],'OrderDate'=>$r["OrderDate"],'Total'=>$r["Total"],
				'UserName'=>$r["UserName"],'UpdatedUser'=>$r["UpdatedUser"],'DriverName'=>$r["DriverName"],
				'shipaddress1'=>$r["shipaddress1"],'shipaddress2'=>$r["shipaddress2"],'phone'=>$r["phone"],'fax'=>$r["fax"]);
		  	}
	 }
	
	  foreach($req as $item)
      {
    	
    	$DateOrdered =$item['OrderDate'];
		$User =$item['UserName'];
		$customerName= $item['CustomerName'];
    	
		$User =$item['UserName'];
		//$UserPrepered =$item['PreparedBy'];
		$UserVerified =$item['UpdatedUser'];
		$Driver =$item['DriverName'];
		$shippAddr= $item['shipaddress1'];// & $item['shipaddress2'];
		$shippAddr2= $item['shipaddress2'];
		$phone= $item['phone'];
		$fax= $item['fax'];
		$shipAddress= "$shippAddr". "\n" ."$shippAddr2 ". "\n" ."$phone". "\n" ."$fax";
		
	  }

class PDF extends FPDF
{
	
// Cargar los datos
function LoadData()
{


}	
	
	
// Cabecera de página
function Header()
{
    global $title;
	global $DateOrdered;
	global $shipAddress;
	global $Driver;
	global $User;
	global $UserVerified;
	global $customerName;
	global $customerName;
	global $customerName;
	
	//$folio = '2';
	$folio =$_GET['FolioNum'];
	
	$w =$this->GetStringWidth($title)+6;
	$this->SetX((210-$w)/2);
    $this->Image('Logo_LosFuertes.png',6,6,52);
	
    // Arial bold 15
    $this->SetFont('Arial','B',18);
    $this->SetLineWidth(0.2);
    // Título
    $this->Cell($w,10,$title,0,1,'R');
	
    	// Salto de línea
    $this->Ln(8);
	$this->SetFont('Arial','',9);
	$this->Cell(50,4,'7020 Troy Hill Drive Units H-J',0,0,'L',0);
	
	$this->Cell(60,5,'',0,0,'C');  
	$this->SetFont('Arial','B',11);
	$this->Cell(40,5,'Date',1,0,'C');
	$this->Cell(40,5,'Invoice #',1,0,'C');
	
	$this->SetFont('Arial','',9);
	$this->Cell(40,5,'','',1,'L',0);   // empty cell with left,bottom, and right borders
	$this->Cell(50,4,'Elkridge, MD 21075',0,0,'L',0);
	$this->Cell(60,5,'',0,0,'C'); 
	$this->SetFont('Arial','',12);
	$this->Cell(40,6,$DateOrdered,1,0,'C',0);
	$this->Cell(40,6,$folio,1,0,'C',0);
	$this->SetFont('Arial','',9);
	$this->Cell(40,5,'','',1,'L',0);   // empty cell with left,bottom, and right borders
	$this->Cell(50,4,'Phone:410-799-5571    Fax:410-799-5581',0,0,'L',0);
	
	// Salto de línea
    $this->Ln(7);
	
	$this->SetFont('Arial','B',10);
	$this->SetDrawColor(0,0,0);
	
	////header -body////
	$this->SetFont('Arial', 'B', 12);
	  
	$this->Cell(30,5,'Customer:',0,0,'L',0);
	$this->Cell(50,5,$customerName,0,1,'L',0);
	$this->Ln(2);
	
	//$this->SetFont('Arial', 'B', 11);
	 $this->SetFillColor(0,0,0);
	
	
	$this->Cell(60,6,'Bill To',1,0,'L',0);
	$this->Cell(60,6,'Shipping To',1,1,'L',0);
	//$this->Cell(60,5,'','0',1,'L',0);   // empty cell with left,bottom, and right borders
	$this->SetFont('Times','',10);

	$x = $this->x;
    $y = $this->y;
    
    $push_right = 0;
	$this->MultiCell($w = 60,5,"$shipAddress",1,'L',0);
	$push_right += $w;
	$this->SetXY($x + $push_right, $y);
	$this->MultiCell($w = 60,5,"$shipAddress",1,'L',0);	
	

	$this->Ln(2);
	
	//$this->Cell(40,5,' ','LTR',0,'L',0);   // empty cell with left,top, and right borders
	$this->SetTextColor(0,0,0);  //color black
	
	$this->SetFont('Arial','B',10);
	$wbody = array(25, 25, 25, 30, 80);   //total 185   set the column title
	$headerBody =array('Created By','Prepared By','Verified By', 'Driver Name','Received By(Customer sign)');
    for($i=0;$i<count($headerBody);$i++)
        $this->Cell($wbody[$i],6,$headerBody[$i],1,0,'C');
    	$this->Ln();
		
	$this->SetFont('Arial','',9);
	$this->Cell($wbody[0],8,$User,1,0,'C',0);  	//$this->Cell(40,5,'Words Here','LR',1,'C',0);  // cell with left and right borders
	$this->Cell($wbody[1],8,'',1,0,'C',0);  //pendiente asignar nombre del preparador
	$this->Cell($wbody[2],8,$UserVerified,1,0,'C',0);
	$this->Cell($wbody[3],8,$Driver,1,0,'C',0);   // empty cell with left,bottom, and right borders
	$this->Cell($wbody[4],8,'',1,1,'C',0); 
	//$this->Cell($wbody[5],10,'','',1,'L',1);
	$this->Ln(2); 
	
	
	/////////
	////header table////
	$this->SetFillColor(217,217,217);
    // Colors, line width and bold font
    //$this->SetFillColor(255,0,0);
    $this->SetTextColor(0);
    $this->SetDrawColor(0,0,0);
    $this->SetLineWidth(.2);
    $this->SetFont('','B');
	$this->SetFillColor(217,217,217);
   
	// Cabecera
	// Anchuras de las columnas
    $w = array(25, 70, 40, 25,25);
	$headerOrder =array('Quantity','Item Description','Comments', 'Unit Price','Total');
    for($i=0;$i<count($headerOrder);$i++)
        $this->Cell($w[$i],6,$headerOrder[$i],1,0,'C',TRUE);
    	$this->Ln();
	
	/////
	
}

function Mybody($headerBody,$req)
{
	
	
	  foreach($req as $item)
      {
    	$customerName= $item['CustomerName'];
    	
		$User =$item['UserName'];
		//$UserPrepered =$item['PreparedBy'];
		$UserVerified =$item['UpdatedUser'];
		$Driver =$item['DriverName'];
		$shippAddr= $item['shipaddress1'];// & $item['shipaddress2'];
		$shippAddr2= $item['shipaddress2'];
		$phone= $item['phone'];
		$fax= $item['fax'];
		$shipAddress= "$shippAddr". "\n" ."$shippAddr2 ". "\n" ."$phone". "\n" ."$fax";
		
	  }
	$this->SetFont('Arial', 'B', 12);
	  
	$this->Cell(30,5,'Customer:',0,0,'L',0);
	$this->Cell(50,5,$customerName,0,1,'L',0);
	$this->Ln(3);
	
	//$this->SetFont('Arial', 'B', 11);
	 $this->SetFillColor(0,0,0);
	
	
	$this->Cell(60,5,'Bill To',1,0,'L',0);
	$this->Cell(60,5,'Shipping To',1,1,'L',0);
	//$this->Cell(60,5,'','0',1,'L',0);   // empty cell with left,bottom, and right borders
	$this->SetFont('Times','',10);

	$x = $this->x;
    $y = $this->y;
    
    $push_right = 0;
	$this->MultiCell($w = 60,5,"$shipAddress",1,'L',0);
	$push_right += $w;
	$this->SetXY($x + $push_right, $y);
	$this->MultiCell($w = 60,5,"$shipAddress",1,'L',0);	
	

	$this->Ln(2);
	
	//$this->Cell(40,5,' ','LTR',0,'L',0);   // empty cell with left,top, and right borders
	$this->SetTextColor(0,0,0);  //color black
	
	$this->SetFont('Arial','B',10);
	$wbody = array(30, 35, 40, 80);   //total 185   set the column title
			
	$this->SetFont('Arial','',10);
	$this->Cell($wbody[0],6,$User,1,0,'C',0);  	//$this->Cell(40,5,'Words Here','LR',1,'C',0);  // cell with left and right borders
	//$this->Cell($wbody[1],10,$UserPrepered,1,0,'C',0);
	$this->Cell($wbody[1],6,$UserVerified,1,0,'C',0);
	$this->Cell($wbody[2],6,$Driver,1,0,'C',0);   // empty cell with left,bottom, and right borders
	$this->Cell($wbody[3],6,'',1,1,'C',0); 
	//$this->Cell($wbody[5],10,'','',1,'L',1);
	$this->Ln(2); 
	
}

function FancyTable($headerOrder,$req) {

	$Totalpdf="";
	//$this->SetFont('Arial', 'B', 10);

    
		
	$this->SetFont('Arial', '', 10);	
	// Color and font restoration
    
    $this->SetTextColor(0);
    $this->SetFont('');
	$this->SetFillColor(242,242,242);
	 $fill = 0;
	 $rows =0;
	 
setlocale(LC_MONETARY, 'en_US');

	 $w = array(25, 70, 40, 25,25);
 	// Datos
	 foreach($req as $item)
            {
            	//"$shippAddr". "\n" ."$shippAddr2 "
            	$PriceDec = sprintf('%15s', $item['UnitPrice'],'\$.2f');
				$TotalUnit =($PriceDec * $item['Quantity']);
				$TotalUnitDec = sprintf('%15s', $TotalUnit,'\$.2f');
				
            	$ProdName = $item['ProductName'];
				$Lot =$item['LotNum'];
				$ProdDesc ="$ProdName". " -" ."$Lot ";
				
            	$this->Cell($w[0],5,$item['Quantity'],'LRB',0,'C',$fill);
				$this->Cell($w[1],5,"$ProdDesc",'LRB',0,'L',$fill);
				$this->Cell($w[2],5,$item['Comments'],'LRB',0,'L',$fill);
				
				$this->Cell($w[3],5,number_format($PriceDec,2),'LRB',0,'R',$fill);   ///price
				$this->Cell($w[4],5,number_format($TotalUnitDec,2),'LRB',1,'R',$fill);
				//$folioSql=  $item['OrderFolio'];
				
				$Totalpdf= $Totalpdf + $item['Quantity'] * $item['UnitPrice'];
				
				$fill = !$fill;
				$rows ++;
				 if ($rows ==31){
				 	$this->AddPage();
					
				}
				
			}
				
$y    =24 - $rows; 

for ($i=1; $i <=$y ; $i++) { 
	$this->Cell($w[0],6,'','LRB',0,'C');
	$this->Cell($w[1],6,'','LRB',0,'L');
	$this->Cell($w[2],6,'','LRB',0);
	$this->Cell($w[3],6,'','LRB',0,'R');
	$this->Cell($w[4],6,'','LRB',1,'R');
}


$TotalDec = sprintf('%15s', $Totalpdf,'\$.2f');  //es necesario esta linea para dar formato a cantidad con cents

$this->SetFont('Arial', 'B', 16);		
$this->SetX(105);  
$this->Cell(40, 10,'Total',1,0,'R');	
$this->Cell(50, 10,'$   '.number_format($TotalDec,2),'LRB',1,'R');	
//$this->SetY(-17);
$this->SetY(-17);
}

function Layout($headerBody,$headerOrder,$req){
	
	$this->AddPage();
	//$this->Mybody($headerBody,$req);
	$this->FancyTable($headerOrder,$req);
	 
}
// Pie de página
function Footer()
{
	$this->SetFont('Arial', '', 7);
	// Arial italic 8
	$msg1 ="The perishable agricultural commodities listed on this invoice are sold subject to the statuory trust authorized by Section 5(c) of the Perishable Agricultural Commodities Act, 1930(7 USC 499(e)(c)). The seller of these commodities retains a trust claim over these commodities, all inventories of food or other products derived from these commodities, and any receivables or proceeds from the sale of these commodities until full payment is received.";
	
	$msg2 ="NOTICE: Past due invoices shall accrue annual interest at the rate af 12% or at the maximum legal rate, whichever is lower. Seller shall be entitled to collect reasonable attorney's fees and expenses as part of and action to collect this invoice. Actual attorney's feed incurred in bringing any action to collect on this invoice and/or enforcing any judgment granted and interest shall be considered as additional sums owed in connection with this transaction. All of our products has full 24hour* quality product guarantee.";
	
	//$msg1= wordwrap( $msg1 );
	$this->SetY(-36);
	$this->MultiCell(185,3.5,"$msg1",1,'L',0);
	$this->MultiCell(185,3.5,"$msg2",1,'L',0);
    
    
    // Posición: a 1,5 cm del final
    $this->SetY(-13);
    // Número de página
    $this->SetFont('Arial', '', 9);
    $this->Cell(0,10,'Page '.$this->PageNo()."/{nb}",0,0,'R');
}

}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$title = 'INVOICE';
$pdf->SetTitle($title);
$headerOrder =array('Quantity','Item Description','Comments', 'Unit Price','Total');
$headerBody =array('User','Verified By', 'Driver Name','Received By');

	
//set default font/colors
$pdf->SetFont('Arial', '', 10);
 
$pdf->SetDrawColor(0,0,0);
//$pdf->AddPage();


//$pdf->AddPage();	
//$pdf->AddPage();	
$pdf->Layout($headerBody, $headerOrder,$req);

$pdf->Output();

?>

