<html>
	
<?php 
include 'core/init.php';
protect_page();

include 'includes/overall/header.php';

$current_url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
$_SESSION['userName']= $user_data['FullName'];
$_SESSION['userGUID']= $user_data['UserGUID'];
?>

 <head>
  <title>Live Add Edit Delete Datatables Records using PHP Ajax</title>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  
  <style>
  body
  {
   margin:0;
   padding:0;
   background-color:#f1f1f1;
 
  }
  .box
  {
   /*width:1270px;*/
   width:inherit;
   padding:7px;
   background-color:#fff;
   border:1px solid #ccc;
   border-radius:5px;
   margin-top:10px;
   box-sizing:border-box;
   
  }
  </style>
 </head>
 <body>
 	
  	
 <div class="container-fluid text-center">
 <div class="row content">
 <div class="col-sm-11 col-md-12 text-left">
 <form name="dateform" action="" method="POST">

  <!-- <div class="container box">-->
   <h1 align="center">Adding Prices to Orders</h1>
   <br />	
 	
 <div class="container-fluid text-center">
 <div class="col-4">
 <FORM>
 <table >
 	<tr >
	 	<td >From:</td>
	 	<td >To:</td>
 	</tr>
 	<tr>
 		<td><input class='form-control' type='date' id = 'orderFrom' name='orderTo' min='2017-07-01'  Value= ''></td>  <!--'".$_SESSION['DateFrom']."'-->
		<td><input class='form-control' type='date' id = 'orderTo' name='orderTo' min='2017-07-01'  Value= ''></td>
		
	</tr>
	<tr></tr>
	<tr>
		<td><input class="btn btn-primary" type="button" name="findOrderBttn" id="findOrderBttn" Value="Find" ></td>
		<td><input class="btn btn-primary" type="button" name="button2" id="BttnClear"Value="Clear Date" ></td>
	</tr>

</table>
</FORM>
</div>	
</div>	


<!-- /////////////////////////////////////////////////////////////////
	////////////this start the table data-->
   <div class="table-responsive">
   <br />
   <!-- <div align="right">
     <button type="button" name="add" id="add" class="btn btn-info">Add</button>
    </div>
    <br />-->
    <div id="alert_message"></div>
    <div ><input type="hidden" id="userId" name="userId" value="<?php echo $user_data['FullName']; ?>" />
    	  <input type="hidden" id="userGUID" name="userGUID" value="<?php echo $user_data['UserGUID']; ?>" /></div>
   
    <table id="orders_data" class="table table-bordered table-striped">
     <thead>
      <tr  class="bg-primary">
       <th>Date</th>
       <th>Folio</th>
       <th>Customer Name</th>
       <th>Qty</th>
       <th>Product Name</th>
       <th>Comments</th>
       <th>Unit Price</th>
       
      </tr>
     </thead>
    </table>
   </div>
  </div>
  </div>
  </div>
  
   
 </body>
</html>

<?php
include 'includes/overall/footer.php';
?>

<script type="text/javascript" language="javascript" >
 $(document).ready(function(){
	 $('#findOrderBttn').click(function () { 
	  	  $('#orders_data').DataTable().destroy(); 
		  fetch_data();
	   });
	   
	  function fetch_data()
	  {
	  	var dateFrom = document.getElementById('orderFrom').value;
	  	var dateTo = document.getElementById('orderTo').value;
	  	
	  	if(dateFrom != '' && dateTo != '')
	   	{  
		    var dataTable = $('#orders_data').DataTable({
		    "processing" : true,
		    "serverSide" : true,
		    "order" : [],
		    "ajax" : {
		     url:"core/functions/fetch_AddPrices.php",
		     type:"POST",
	    	 data:{dateFrom:dateFrom, dateTo:dateTo}
		    }
		   });
		 }else	{
	    	 alert("Both Dates are required");
	    	 $('#orders_data').DataTable().destroy();
	   	 }
	  }
  	
  	  $(document).on('blur', '.update', function(){
		   var id = $(this).data("id");
		   var value = this.value;
		   var column_name = $(this).data("column");
		 //  alert(value);
		  // var value = $(this).text();
		   update_data(id, column_name, value);
	   
  	   });
  	   
  function update_data(id, column_name, value)
  {
	  	var userId = document.getElementById("userId").value; 
	  	var userGUID = document.getElementById("userGUID").value;
	  	
	  //	alert (userGUID);
	    $.ajax({
	    url:"core/functions/UpdateDetailsOrder.php",
	    method:"POST",
	    data:{id:id, column_name:column_name, value:value,userId:userId,userGUID:userGUID},
	    success:function(data)
	    {
	    	if (data == 'Data Updated'){
	    		$('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
		    
			     $('#orders_data').DataTable().destroy();
			     fetch_data();
	    	}else{
	    		$('#alert_message').html('<div class="alert alert-danger">'+data+'</div>');
		    
			     $('#orders_data').DataTable().destroy();
			     fetch_data();
	    	}
		     
	    }
	   });
	   setInterval(function(){
	    $('#alert_message').html('');
	   }, 7000);
	  }
	

  	
  	

 });
 
 
 //////////////////////////////////////////////////////
 
</script>
